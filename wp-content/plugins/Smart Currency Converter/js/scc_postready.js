
var scc_init_popup_msg = "<div class='scc_initpop'><span class='scc_closeBtn'>X</span><header><h2>" + settings.init_message + "</h2><p>" + settings.init_message2 + "</p></header></div>"



var scc_init_popups = function(){

	var scc_firstPage = false;

	if(!(jQuery.cookie("notFirstTime"))){
		scc_firstPage = true;
		jQuery.cookie("notFirstTime", "1");
	}


	if(scc_firstPage && settings.show_init_pop == '1'){

		jQuery('body').append(scc_init_popup_msg);

		jQuery('.scc_initpop').bPopup({transition: 'slideDown', position: [0, 0],  followSpeed: 0, closeClass:"scc_closeBtn"});

	}


	scc_widget();
	

};

function scc_widget(){

	if( jQuery('select#ch_scc_woo_widget_select').length ){

		var country = localStorage['scc_countryCode'];
		var countries = jQuery.scc_getAllCountries();

		var im = '';
		var optionsHTML = '';

		for (var key in countries){

//			im += "<img src='http://www.worldatlas.com/webimage/flags/countrys/zzzflags/" + key.toLowerCase()  + "large.gif'/> " + countries[key].countryName + " <br>";
			optionsHTML += "<option value='" + key + "' " + ((key==country)?"selected":"") + " >" + countries[key].countryName + " (" + countries[key].currencyCode + ")</option>";
		}

		

		jQuery('select#ch_scc_woo_widget_select').html(optionsHTML);

//		jQuery('select#ch_scc_woo_widget_select').parent().append(im);

		jQuery('select#ch_scc_woo_widget_select').chosen();


		jQuery('button#scc_widget_upd').click(function(){

	

			localStorage['scc_countryCode'] = jQuery('select#ch_scc_woo_widget_select').val();


			location.reload(true);
		});


}
}

var jscc_options = {

	replaceOriginalPrice : settings.replaceOriginalPrice==='1',
	baseCurrency   : settings.baseCurrency,
	targets   : settings.targets.split(','), 
	showFallbackOnAutodetectFailure   : settings.showFallbackOnAutodetectFailure==='1',
	autodetectFallbackCurrency   : settings.autodetectFallbackCurrency,
	decimalPrecision   : (settings.decimalPrecision==='1')?2:0,
	thousandSeperator   : (settings.thousandSeperator==='1')?',':'',
	tooltipTheme   : settings.tooltipTheme,
	tooltipAnimation   : settings.tooltipAnimation,
	animationDuration   : settings.animationDuration,
	showTooltipArrow   : settings.showTooltipArrow==='1',
	tooltipPosition   : settings.tooltipPosition,
	tooltipShowDelay   : settings.tooltipShowDelay,
	exchangeRateUpdateInterval   : settings.exchangeRateUpdateInterval,
	touchFriendly   : settings.touchFriendly==='1',
	hideTooltipToNativeVisitor   : settings.hideTooltipToNativeVisitor==='1',
	debugMode : settings.debugMode==='1',
	onFinish : scc_init_popups,
	tooltipAlwaysOpen: settings.tooltipAlwaysOpen ==='1',
	exchangeRateOverrides : settings.exchange_rates,
	gearUrl : settings.gearUrl,
	replacedContentFormat: settings.replacedContentFormat
};

jQuery(document).ready(function(){


	var selectors = settings.customClasses.split(',');

	jQuery.each(selectors, function (index, element){
		selectors[index] = selectors[index].replace(/^\s+|\s+$/g,'');
	});

	if(settings.debugMode==='1'){
		console.log("Custom selectors are: ")
		console.log(selectors);	
	}
	

	window.setTimeout( function(){

		jQuery('.amount').currencyConverter(jscc_options);

		jQuery.each(selectors, function (index, element){

			jQuery(selectors[index]).currencyConverter(jscc_options);

		});


	} ,500);

	
	
});
