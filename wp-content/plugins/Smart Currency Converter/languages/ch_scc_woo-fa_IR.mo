��    I      d  a   �      0     1     >     L     U  	   g     q  
   �     �  
   �  0   �     �     �     �               $     1     C     L     d     v     �     �     �     �     �     �     �       	     
        )  �   8     �     �     �      �  `   	     �	     �	  0   �	     �	     �	     �	  O   
     T
     i
     ~
     �
     �
     �
  #   �
     �
     �
     �
     �
       
   ,  
   7     B  '   ]  (   �     �     �     �     �     �     �  	   �     �          !  �  ;     �     �          '     E     T     s     |     �  i   �  N     
   e     p     �     �     �     �     �      �           ?  /   Z     �     �  0   �     �     �               4     @     N  �   `          "     ?  ,   O  �   |  2   $     W  u   \     �     �     �  x        �     �     �     �     �       G     "   ^     �  
   �  )   �  (   �     �     �  1     N   I  6   �     �     �     �                    1     J  )   b     �     >       #   .   0   %   E                           D   &   1   8   H           +         ,          9               3   5   (                =   I       "   G      C   6       $   B   ;       	         7   -       2   4         ?                            *   
      @   :                )      /       A       F         '                        <   !    Add new rate Add this rate Advanced Advanced Settings Animation Animation Duration Appearance Auto detect Autodetect Automatically open all tooltips when page loads. Bigger for slower animation. Bottom Bottom-left Bottom-right Choose currency Comma Format Converted amount. Currency Currency Chooser Widget Currency Settings Custom Exchange Rates Custom Price Selectors Display Mode Display Settings Enable Debugging Mode Every 10 Minutes Every 30 Minutes Every 6 hours Every Minute Every day Every hour Exchange Rates Exchange rates that are not specified here will be fetched from <a href="http://finance.yahoo.com/currency-converter"  target="_blank">Yahoo Finance</a>. Fallback Fallback Currency Fixed currencies HTML and inline CSS are allowed. Hide the tooltip to visitors whose currency is same as the default currency of the website (%s). Hide to Native Visitor Left Lets the visitor choose their preferred currency Miscellaneous Miscellaneous Settings Open All Tooltips Please give some numeric value for exchange rate. For example 3 or 4.56 or 0.67 Popup Message Line 1 Popup Message Line 2 Popup Settings Popups Position Remove this rate Replace Original Price (No Tooltip) Replaced Content Format Right Save See documentation for details. Show 2 digits after dot Show Arrow Show Delay Show Initial Popup Message Show debug messages in browser console. Smart Currency Converter for Woocommerce Target Currencies The default price. Theme Title: Top Top-left Top-right Touch Friendly Update Exchange Rate Cache Your Custom Exchange Rate Project-Id-Version: Smart Currency Converter for Woocommerce
POT-Creation-Date: 2016-09-08 14:53+0430
PO-Revision-Date: 2016-09-08 15:22+0430
Last-Translator: 
Language-Team: 
Language: fa_IR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.4
X-Poedit-Basepath: ..
X-Poedit-WPHeader: index.php
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 افزودن نرخ جدید افزودن نرخ جدید پیشرفته تنظیمات پیشرفته انیمیشن مدت زمان انیمیشن ظاهر تشخیص خودکار تشخیص خودکار باز شدن خودکار راهنما ابزار زمانی که صفحه بارگزاری می شود. مقدار بزرگتر اجرای انیمیشن را کندتر می کند. پایین پایین سمت چپ پایین سمت راست انتخاب واحد پول فرمت کاما مقدار تبدیل شده. واحد پول ابزارک انتخاب ارز تنظیمات واحد پول نرخ ارز سفارشی انتخاب کننده هزینه سفارشی حالت نمایش تنظیمات نمایش فعال کردن حالت اشکال زدایی هر 10 دقیقه هر 30 دقیقه هر 6 ساعت در هر دقیقه هر روز هر ساعت نرخ ارزها نرخ ارز که در اینجا مشخص نشده است را از ذهن <a href="http://finance.yahoo.com/currency-converter"  target="_blank">یاهو امور مالی</a> . جانشین واحد پول جانشین ارز ثابت HTML و CSS درون خطی مجاز است. مخفی کردن راهنمای ابزار به بازدید کنندگان که از همان ارز پیش فرض  فروشگاه (%s) استفاده می کنند. مخفی به بازدید کنندگان بومی چپ به بازدید کنندگان اجازه می دهد تا ارز مورد نظر خود را انتخاب کنند متفرقه تنظیمات دیگر نمایش همه Tooltip ها لطفا نرخ ارز را به صورت عددی وارد نمایید. به عنوان مثال 3 یا 4.56 یا 0.67 متن خط اول پاپ آپ متن خط دوم پاپ آپ تنظیمات پاپ آپ پاپ آپ موقعیت حذف این نرخ جایگزین هزینه اصلی (بدون راهنمای ابزار) فرمت مقدار جایگزین راست ذخیره مشاهده مستندات پیش فرض نمایش 2 رقم بعد از نقطه نمایش فِلش نمایش با تاخیر نمایش پیام های پاپ آپ اولیه نمایش پیام های اشکال زدایی در کنسول مرورگر. مبدل هوشمند واحد پولی ووکامرس واحد پول هدف قیمت پیش فرض. قالب عنوان: بالا بالا سمت چپ بالا سمت راست لمسی دوستانه به روز رسانی نرخ ارز کش نرخ ارز سفارشی 