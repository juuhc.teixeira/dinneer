<?php

function init_options(){


	if( false == get_option( 'scc_currency_options' ) ) {	
		add_option( 'scc_currency_options' );

		update_option('scc_currency_options', array(


			"targets" => "autodetect",
			"showFallbackOnAutodetectFailure" => "1",
			"autodetectFallbackCurrency" => "USD",
			"decimalPrecision" => "0",
			"thousandSeperator" => "1"

			));

	}

	if( false == get_option( 'scc_theme_options' ) ) {	
		add_option( 'scc_theme_options' );

		update_option('scc_theme_options', array(

			"replaceOriginalPrice" => "0",
			"tooltipTheme" => "shadow",
			"tooltipAnimation" => "fade",
			"animationDuration" => "300",
			"showTooltipArrow" => "1",
			"tooltipPosition" => "top",
			"tooltipShowDelay" => "100",
			"replacedContentFormat" => "[convertedCurrencyCode] [convertedAmount]"
			
			));

	}

	if( false == get_option( 'scc_misc_options' ) ) {	
		add_option( 'scc_misc_options' );

		update_option('scc_misc_options', array(

			"exchangeRateUpdateInterval" => "30",
			"touchFriendly" => "1",
			"hideTooltipToNativeVisitor" => "0"
			));

	}

	if( false == get_option( 'scc_adv_options' ) ) {	
		add_option( 'scc_adv_options' );

		update_option('scc_adv_options', array(
			"debugMode" => "0",
			"customClasses" => ""
			));

	}

	if( false == get_option( 'scc_popup_options' ) ) {	
		add_option( 'scc_popup_options' );

		update_option('scc_popup_options', array(

			"show_init_pop" => "1",
			"show_config_pop" => "1",
			"init_message" =>  'Keep your mouse pointer over the prices to see them converted',
			"init_message2" =>     'You\'d still have to pay in the stores default currency though.' 

		    ));

	}

	if( false == get_option( 'scc_exrate_options' ) ) {	
		add_option( 'scc_exrate_options' );

		update_option('scc_exrate_options', "");
	}

	add_settings_section(
		'scc_currency_section',	
		__('Currency Settings','ch_scc_woo'),	
		'scc_options_callback',	
		'scc_currency_options'		
	);

	add_settings_section(
		'scc_theme_section',	
		__('Display Settings','ch_scc_woo'),	
		'scc_options_callback',	
		'scc_theme_options'		
	);

	add_settings_section(
		'scc_misc_section',	
		__('Miscellaneous Settings','ch_scc_woo'),	
		'scc_options_callback',	
		'scc_misc_options'		
	);

	add_settings_section(
		'scc_adv_section',	
		__('Advanced Settings','ch_scc_woo'),	
		'scc_adv_options_callback',	
		'scc_adv_options'		
	);

	add_settings_section(
		'scc_popup_section',	
		__('Popup Settings','ch_scc_woo'),	
		'scc_options_callback',	
		'scc_popup_options'		
	);

	add_settings_section(
		'scc_exrate_section',	
		__('Custom Exchange Rates','ch_scc_woo'),	
		'scc_exrate_callback',	
		'scc_exrate_options'		
	);



//	add_settings_field('baseCurrency',						'Base Currency'		,		'render_scc_baseCurrency',						'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('targets',							__('Target Currencies', 'ch_scc_woo'),					'render_scc_targets',							'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('showFallbackOnAutodetectFailure',	__('Fallback', 'ch_scc_woo'),								'render_scc_showFallbackOnAutodetectFailure',	'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('autodetectFallbackCurrency',		__('Fallback Currency', 'ch_scc_woo'),					'render_scc_autodetectFallbackCurrency',		'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('decimalPrecision',					__('Show 2 digits after dot', 'ch_scc_woo'),				'render_scc_decimalPrecision',					'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('thousandSeperator',					__('Comma Format', 'ch_scc_woo'),							'render_scc_thousandSeperator',					'scc_currency_options',			'scc_currency_section'			);
	add_settings_field('replaceOriginalPrice',				__('Display Mode', 'ch_scc_woo'),							'render_scc_replaceOriginalPrice',				'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('tooltipTheme',						__('Theme', 'ch_scc_woo'),								'render_scc_tooltipTheme',						'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('tooltipAnimation',					__('Animation', 'ch_scc_woo'),							'render_scc_tooltipAnimation',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('animationDuration',					__('Animation Duration', 'ch_scc_woo'),					'render_scc_animationDuration',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('showTooltipArrow',					__('Show Arrow', 'ch_scc_woo'),							'render_scc_showTooltipArrow',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('tooltipPosition',					__('Position', 'ch_scc_woo'),								'render_scc_tooltipPosition',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('tooltipShowDelay',					__('Show Delay', 'ch_scc_woo'),							'render_scc_tooltipShowDelay',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('tooltipAlwaysOpen',					__('Open All Tooltips', 'ch_scc_woo'),					'render_scc_tooltipAlwaysOpen',					'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('replacedContentFormat',				__('Replaced Content Format', 'ch_scc_woo'),				'render_scc_replacedContentFormat',				'scc_theme_options',			'scc_theme_section'			);
	add_settings_field('exchangeRateUpdateInterval',		__('Update Exchange Rate Cache', 'ch_scc_woo'),			'render_scc_exchangeRateUpdateInterval',		'scc_misc_options',				'scc_misc_section'			);
	add_settings_field('hideTooltipToNativeVisitor',		__('Hide to Native Visitor', 'ch_scc_woo'),				'render_scc_hideTooltipToNativeVisitor',		'scc_misc_options',				'scc_misc_section'			);
	add_settings_field('touchFriendly',						__('Touch Friendly', 'ch_scc_woo'),						'render_scc_touchFriendly',						'scc_misc_options',				'scc_misc_section'			);
	add_settings_field('debugMode',							__('Enable Debugging Mode', 'ch_scc_woo'),				'render_scc_debugMode',							'scc_adv_options',				'scc_adv_section'			);
	add_settings_field('customClasses',						__('Custom Price Selectors', 'ch_scc_woo'),				'render_scc_customClasses',						'scc_adv_options',				'scc_adv_section'			);
	add_settings_field('show_init_pop',						__('Show Initial Popup Message', 'ch_scc_woo'),			'render_scc_show_init_pop',					'scc_popup_options',			'scc_popup_section'			);	
	add_settings_field('init_message',						__('Popup Message Line 1', 'ch_scc_woo'),					'render_scc_init_message',						'scc_popup_options',			'scc_popup_section'			);
	add_settings_field('init_message2',						__('Popup Message Line 2', 'ch_scc_woo'),					'render_scc_init_message2',						'scc_popup_options',			'scc_popup_section'			);

//	add_settings_field('exchange_rates',					'Your custom exchange rates',	'render_scc_exrates',					'scc_exrate_options',			'scc_exrate_section'			);



	register_setting(
		'scc_currency_options',
		'scc_currency_options'
	);

	register_setting(
		'scc_theme_options',
		'scc_theme_options'
	);

	register_setting(
		'scc_misc_options',
		'scc_misc_options'
	);

	register_setting(
		'scc_adv_options',
		'scc_adv_options'
	);

	register_setting(
		'scc_popup_options',
		'scc_popup_options'
	);

	register_setting(
		'scc_exrate_options',
		'scc_exrate_options'
	);


}

?>