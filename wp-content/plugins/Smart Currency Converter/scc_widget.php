<?php

class SCC_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(

            'scc_widget', 


            __('Currency Chooser Widget', 'ch_scc_woo'), 


            array( 'description' => __( 'Lets the visitor choose their preferred currency', 'ch_scc_woo' ), ) 
            );
    }


    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];


        ?>

        <select id='ch_scc_woo_widget_select'>

        </select>
        <button id="scc_widget_upd"><?php _e('Save', 'ch_scc_woo');?></button>
        <?php



        echo $args['after_widget'];
    }


    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'Choose currency', 'ch_scc_woo' );
        }

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
    

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} 





function scc_load_widget() {
    register_widget( 'SCC_Widget' );
}

?>