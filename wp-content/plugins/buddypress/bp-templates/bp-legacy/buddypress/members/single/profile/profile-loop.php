<?php
/**
 * BuddyPress - Members Profile Loop
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
do_action( 'bp_before_profile_loop_content' ); ?>
<style>
    p{
        margin-left: 10px;
    }
    
    .panel-body p {
        width: 85%;
    }
    
    #item-nav{
        display: none;
    }

    
    .id-0 strong {
        display: none;
    }
    
    .anuncio h4 {
        color: #fff;
        text-align: -webkit-center;
    }
    
    div#anuncio {
        margin-top: -11%;
    }
</style>
<?php if ( bp_has_profile() ) : ?>

	<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php

			/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
			do_action( 'bp_before_profile_field_content' ); ?>

			<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">

				<h2><?php bp_the_profile_group_name(); ?></h2>

				<div class="row">
                                    <div class="col-md-3" >
                                        <div class="panel panel-default">
                                            <?php $id = 0;?>
                                            <?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>
                                                    <?php if ( bp_field_has_data() ) : ?>                                                    
                                                            <div <?php bp_field_css_class(); ?>>                                                            
                                                                <div class="panel-body id-<?php echo $id; ?>" style="    padding: 0 6px;">
                                                                    <strong><?php bp_the_profile_field_name(); ?>: </strong><?php bp_the_profile_field_value(); ?>
                                                                    
                                                    <hr>
                                                                </div>
                                                            </div>

                                                    <?php endif; ?>
                                                    <?php

                                                    /**
                                                     * Fires after the display of a field table row for profile data.
                                                     *
                                                     * @since 1.1.0
                                                     */
                                                    do_action( 'bp_profile_field_item' ); ?>
                                                    <?php $id++; ?>
                                            <?php endwhile; ?>
                                        </div>

                                    </div>
                                    
			<?php

			/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
			do_action( 'bp_after_profile_field_content' ); ?>
                        <?php

    /* the loop args */
    $args = array( 
        'author' => bp_displayed_user_id(),
        'post_type' => 'st_activity'
    );
    query_posts( $args );
    /* end loop args */
    
    /*the loop */
    ?>
                                    
<div class="col-md-8 " id="anuncio">
    <h3>Anúncios</h3>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-md-4">
            <div class="anuncio">
                <a href="<?php the_permalink();?>"> 
                    <div class="title-anuncio">
                        <h4><?php the_title();?> </h4>
                    </div>
                    <div class="img-profile-anuncio">
                        <?php  the_post_thumbnail(); ?>
                    </div>
                </a>
            </div>
        </div>        
<?php
        // do something amazing
    endwhile; else:
        ?><h3> no posts found so do something less amazing</h3> <?php
    endif;
    
    wp_reset_query();
    

?>
        <div class="clearfix"></div>
    </div>
                                </div>
                            </div>
		<?php endif; ?>

	<?php endwhile; ?>

	<?php

	/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
	do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/profile/profile-wp.php */
do_action( 'bp_after_profile_loop_content' ); ?>
