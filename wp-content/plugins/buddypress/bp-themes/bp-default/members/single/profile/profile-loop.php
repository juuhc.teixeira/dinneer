<?php do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) : ?>

	<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php do_action( 'bp_before_profile_field_content' ); ?>

			<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">

				<h4><?php bp_the_profile_group_name(); ?></h4>

                                <div class="row">
                                    <h3>An�ncios</h3>
<?php


    /* the loop args */
    $args = array( 
        'author' => bp_displayed_user_id(),
        'post_type' => 'st_activity'
    );
    query_posts( $args );
    /* end loop args */
    
    /*the loop */
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-md-6">
            <div class="anuncio">
                <a href="<?php the_permalink();?>"> 
                    <div class="title-anuncio">
                        <h3><?php the_title();?> </h3>
                    </div>
                    <div class="img">
                        <?php  the_post_thumbnail(); ?>
                    </div>
                </a>
            </div>
        </div>
        
<?php
        // do something amazing
    endwhile; else:
        ?><h3> no posts found so do something less amazing</h3> <?php
    endif;
    
    wp_reset_query();

?>
					<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

						<?php if ( bp_field_has_data() ) : ?>
                                                    <div class="col-md-6">
							<div <?php bp_field_css_class(); ?>>

								<div class="label"><?php bp_the_profile_field_name(); ?></div>

								<div class="data"><?php bp_the_profile_field_value(); ?></div>

							</div>
                                                    </div>

						<?php endif; ?>

						<?php do_action( 'bp_profile_field_item' ); ?>

					<?php endwhile; ?>
                                    

				</div>
			</div>

			<?php do_action( 'bp_after_profile_field_content' ); ?>

		<?php endif; ?>

	<?php endwhile; ?>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
