<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Single activity
 *
 * Created by ShineTheme
 *
 */
global $st_post_content;
if ( have_posts() ) {
    while (have_posts()) {
        the_post();
        ob_start();
        the_content( );
        $st_post_content=@ob_get_clean();
    }
}
get_header();

$detail_layout=apply_filters('st_activity_detail_layout',st()->get_option('activity_layout'));
if(get_post_meta($detail_layout , 'is_breadcrumb' , true) !=='off'){
    get_template_part('breadcrumb');
}
$layout_class = get_post_meta($detail_layout , 'layout_size' , true);
if (!$layout_class) $layout_class = "container";
?>
<div itemscope itemtype="http://schema.org/TouristAttraction">
	<div class="<?php echo balanceTags($layout_class) ; ?>">
		<div class="booking-item-details no-border-top">
			<header class="booking-item-header">
				<div class="row">
					<div class="col-md-9">
						<?php echo st()->load_template('activity/elements/header'); ?>
					</div>
					<div class="col-md-3 text-right price_activity">
						<p class="booking-item-header-price">
						<?php echo STActivity::get_price_html(get_the_ID(),false,'<br>'); ?>
						</p>
					</div>
				</div>
			</header>
			<?php
			if($detail_layout)
			{
				$content=STTemplate::get_vc_pagecontent($detail_layout);
				echo balanceTags( $content);
			}else{
				echo st()->load_template('activity/single','default');
			}
                        
                        
			?>
		
<!--
                    
<div class="info-activity">
    <div class="info">
        <span class="head"><i class="fa fa-info"></i> <?php echo __('Activity type', ST_TEXTDOMAIN) ?> : </span>
        <span><?php if($type_activity == 'daily_activity') echo __('Daily Activity', ST_TEXTDOMAIN); else echo __('Specific Date', ST_TEXTDOMAIN) ?></span>
    </div>
<?php
    $activity_time = get_post_meta( get_the_ID() , 'activity-time' , true );
    if(!empty( $activity_time )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-clock-o"></i> <?php st_the_language( 'activity_time' ) ?> : </span>
            <span><?php echo esc_html( $activity_time ); ?> </span>
        </div>
<?php endif; ?>
<?php
    $hora_inicio = get_post_meta( get_the_ID(), 'hora_inicio' , true);

?>
    <div class="info">
        <span class="head"><i class="fa fa-clock-o"></i> <?php _e('Start in',ST_TEXTDOMAIN) ?> : </span>
        <span><?php echo esc_html( $hora_inicio ); ?> </span>
    </div>
<?php
    $duration = get_post_meta( get_the_ID() , 'duration' , true );
    if(!empty( $duration )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-clock-o"></i> <?php _e('Duration',ST_TEXTDOMAIN) ?> : </span>
            <span><?php echo esc_html( $duration ); ?> </span>
        </div>
<?php endif; ?>
    <div class="info">
        <span class="head"><i class="fa fa-users"></i> <?php echo __('Maximum number of people', ST_TEXTDOMAIN); ?> : </span>
        <span><?php 
            if( !$max_people || $max_people == 0 ){
                $max_people = __('Unlimited', ST_TEXTDOMAIN);
            }
            echo $max_people; 
        ?></span>
    </div>
<?php
    $facilities = get_post_meta( get_the_ID() , 'venue-facilities' , true );
    if(!empty( $facilities )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-cogs"></i> <?php st_the_language( 'venue_facilities' ) ?> : </span>
            <span><?php echo esc_html( $facilities ); ?> </span>
        </div>
<?php endif; ?>
</div>
-->
		</div>
	</div>
	<!--Review Rich Snippets-->
	<div itemprop="aggregateRating" class="hidden" itemscope itemtype="http://schema.org/AggregateRating">
		<div><?php _e('Book rating:',ST_TEXTDOMAIN)?>
			<?php printf(__('%s out of %s with %s ratings',ST_TEXTDOMAIN),
			'<span itemprop="ratingValue">'.STReview::get_avg_rate().'</span>',
			'<span itemprop="bestRating">5</span>',
			'<span itemprop="ratingCount">'.get_comments_number().'</span>'
			); ?>
		</div>
	</div>
	<!--End Review Rich Snippets-->


</div>
	<span class="hidden st_single_activity"></span>
<style>
    .fixed-top {
        position: fixed;
        top: 0;
        z-index: 99;
        padding: 10px;
        }

        @media screen and (max-width: 767px){
        .fixed-top {
            min-width: 50%;
          }
        }
</style>
  <script>
   (function($){
      // Get the element to fixed
      var offset = $( '.fixed' );
 
      $(window).scroll(function () {
 
        // Fixed it when scrolling to bottom
        if (offset.offset().top > $(window).scrollTop()) {
            offset.addClass( 'fixed-top' );
 
        // or remove fixed when back to top
        } else if ( $(window).scrollTop() == 0 ) {
            offset.removeClass( 'main-fixed' );
        }
      });
    })(jQuery);
  </script>
<?php get_footer( ) ?>