<?php

    /* the loop args */
    $args = array( 
        'author' => bp_displayed_user_id(),
        'post_type' => 'st_activity'
    );
    query_posts( $args );
    /* end loop args */
    
    /*the loop */
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="col-md-6">
            <div class="anuncio">
                <a href="<?php the_permalink();?>"> 
                    <div class="title-anuncio">
                        <h3><?php the_title();?> </h3>
                    </div>
                    <div class="img">
                        <?php  the_post_thumbnail(); ?>
                    </div>
                </a>
            </div>
        </div>
        
<?php
        // do something amazing
    endwhile; else:
        ?><h3> no posts found so do something less amazing</h3> <?php
    endif;
    
    wp_reset_query();

?>