<?php
    $args = array( 'author' => bp_displayed_user_id(),
                    'post_type' => 'post'
            );
    query_posts( $args );
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<h3> <?php the_title();?> </h3>
<p><?php the_excerption();?></p>
<?php
        // do something amazing
    endwhile; else:
        ?><h3> no posts found so do something less amazing</h3> <?php
    endif;
    wp_reset_postdata();

?>

