<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * function
 *
 * Created by ShineTheme
 *
 */


// Filter wp_nav_menu() to add profile link
/*add_filter( 'wp_nav_menu_items', 'my_nav_menu_profile_link' );
function my_nav_menu_profile_link($menu) {
        if (!is_user_logged_in())
                return $menu;
        else
                $profilelink = '<li><a href="' . bp_loggedin_user_domain( '/' ) . '">' . __('Seu Perfil') . '</a></li>';
                $menu = $menu . $profilelink;
                return $menu;
}
*/
wp_register_script( 'validation', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array( 'jquery' ) );
wp_enqueue_script( 'validation' );

function my_bp_nav_adder() {
    global $bp;
    bp_core_new_nav_item(
            array(
                    'name'                => __( 'Anúncios', 'buddypress' ),
                    'slug'                => 'user-anuncios',
                    'position'            => 90,
                    'screen_function'     => 'postsdisplay',
                    'default_subnav_slug' => 'user-anuncios',
                    'parent_url'          => $bp->loggedin_user->domain . $bp->slug . '/',
                    'parent_slug'         => $bp->slug
            ) );
}
 
function postsdisplay() {
    //add title and content here - last is to call the members user-posts.php template
    add_action( 'bp_template_title', 'my_posts_page_function_to_show_screen_title' );
    add_action( 'bp_template_content', 'my_posts_page_function_to_show_screen_content' );
    bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members' ) );
}
 
/*function my_posts_page_function_to_show_screen_title() {
    echo 'My new Page Title';
}
 
function my_posts_page_function_to_show_screen_content() {
    echo 'My Tab content here';
 
}*/
 
add_action( 'bp_setup_nav', 'my_bp_nav_adder', 50 );

add_filter( 'woocommerce_checkout_fields' , 'removendo_campos_checkout' );
 


function author_info(){
    $name = get_the_author();
    $url = bp_core_get_user_domain( get_the_author_meta( 'ID' ) );
    $user = get_avatar( get_the_author_meta( 'ID' ), 60 );
    //var_dump(get_the_author_meta( 'ID' ));
    ///is echo  '<br />';
    //$perfil = bp_profile_field_data( array('user_id'=>get_the_author_meta( 'ID' ),'field'=>'Sobre' ));
   // $perfil = bp_profile_field_data( array('user_id'=>7 ));
    $perfil = bp_get_profile_field_data( array('user_id'=>get_the_author_meta( 'ID' ),'field'=>'Sobre' ));
    
    //var_dump($perfil);
    
    if(empty($perfil) || $perfil== ""){
        $perfil_completo = '<p> Este anfitrião ainda não preencheu todo o seu perfil. </p>';
    }else{
        $perfil_completo = $perfil;
    }
    $meta = '<div class="row"> '
                . '<div class="container">'
                    . '<div class="col-md-3">'
                        . '<a href="'. $url .'" title="Ir para o perfil do Anfitrião" class="thumbnail">'. $user .'</a><br>
                    </div>
                    <div class="col-md-9">
                        <h3><a href="'.$url.'" title="Ir para o perfil do Anfitrião"> '.$name.' </a></h3>
                            '. $perfil_completo. ' 
                        <br><br>
                        <a href="'.$url.' " class="btn btn-primary"> Visitar Perfil </a>'
                    . '</div>'
                . '</div>'
            . '</div>';

    return  $meta;
}
    add_shortcode( 'author_info', 'author_info' );
    
function st_dessert(){
    $st_dessert= get_post_meta( get_the_ID(), 'st_dessert' , true);
    if(!empty($st_dessert)){
        $text = esc_html($st_dessert);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $st_dessert;
    
    return $layout;
}
add_shortcode('st_dessert', 'st_dessert');

function st_drinks(){
    $st_drinks= get_post_meta( get_the_ID(), 'st_drinks' , true);
    if(!empty($st_drinks)){
        $text = esc_html($st_drinks);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $st_drinks;
    
    return $layout;
}
add_shortcode('st_drinks', 'st_drinks');

function st_maincourse(){
    $st_maincourse= get_post_meta( get_the_ID(), 'st_maincourse' , true);
    if(!empty($st_maincourse)){
        $text = esc_html($st_maincourse);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $st_maincourse;
    
    return $layout;
}
add_shortcode('st_maincourse', 'st_maincourse');

function st_content(){
    $st_content= get_post_meta( get_the_ID(), 'st_content' , true);
    if(!empty($st_content)){
        $text = esc_html($st_content);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $st_content;
    
    return $layout;
}
add_shortcode('st_content', 'st_content');

function st_entrada(){
    $st_entrada= get_post_meta( get_the_ID(), 'st_entre' , true);
    if(!empty($st_entrada)){
        $text = esc_html($st_entrada);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $st_entrada;
    
    return $layout;
}
add_shortcode('st_entrada', 'st_entrada');

function adult_price(){
    $adult_price= get_post_meta( get_the_ID(), 'adult_price' , true);
    if(!empty($adult_price)){
        $text = esc_html($adult_price);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $adult_price;
    
    return $layout;
}
add_shortcode('adult_price', 'adult_price');

function currency(){
    $currency= get_post_meta( get_the_ID(), 'currency' , true);
    if(!empty($currency)){
        $text = esc_html($currency);
    }else{
        $text = 'Campo vazio!';
    }
    
    $layout = $currency;
    
    return $layout;
}
add_shortcode('currency', 'currency');


function activity_time(){
    $activity_time = get_post_meta( get_the_ID() , 'duration' , true );
    if(!empty( $activity_time )){
        $title_time = esc_html( $activity_time );
    }else{
        $title_time = 'Campo vazio!';
    }
    
    $layout = 
            '<center>
                <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                    <li>
                       <i class="fa fa-clock-o"></i>
                    <span class="booking-item-feature-title">Starts at: '. $title_time.'</span>
                
                </li>
                
            </ul></center>
                ';
    
    return $layout;
}
add_shortcode( 'activity_time', 'activity_time' );

function number_people(){
    $max_people = intval(get_post_meta( get_the_ID(), 'max_people', true));

    if( !$max_people || $max_people == 0 ){
                $max_people = __('Unlimited', ST_TEXTDOMAIN);
    }
            
             
    $layout = 
            '<center>
                <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                    <li>
                       <i class="fa fa-users"></i>
                    <span class="booking-item-feature-title">Serve '. $max_people.' pessoas.</span>
                
                </li>
                
            </ul></center>
                ';
            return $layout; 
  
}
add_shortcode('number_people', 'number_people');

function people(){
    $people = intval(get_post_meta( get_the_ID(), 'max_people', true));

    if( !$people || $people == 0 ){
                $people = __('Unlimited', ST_TEXTDOMAIN);
    }else{
        $layout =  '<span class="booking-item-feature-title">Max People: '. $people.'</span>'; 
    }
 
    return $layout; 
  
}
add_shortcode('people', 'people');

function activity_duration(){
    $duration = get_post_meta( get_the_ID() , 'activity-time' , true );
    $layout = ' <center>
                <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                    <li>
                       <i class="fa fa-clock-o"></i>
                    <span class="booking-item-feature-title">Ends in: '. $duration.'</span>
                
                </li>
                
            </ul></center>';
    
    return $layout;
}
add_shortcode( 'activity_duration', 'activity_duration' );

if(!defined('ST_TEXTDOMAIN'))
define ('ST_TEXTDOMAIN','traveler');

if(!defined('ST_TRAVELER_VERSION'))
{
    $theme=wp_get_theme();
	if($theme->parent())
	{
		$theme=$theme->parent();
	}
    define ('ST_TRAVELER_VERSION',$theme->get( 'Version' ));
}


$status=load_theme_textdomain(ST_TEXTDOMAIN,get_stylesheet_directory().'/language');

get_template_part('inc/class.traveler');