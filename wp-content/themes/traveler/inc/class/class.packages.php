<?php
/**
 *@since 1.3.1
 *    Class Packages
 **/
global $pack_orderdata;
if (!class_exists('STPackages')) {
    class STPackages extends TravelerObject
    {
        private static $instance;
        public function __construct()
        {
            self::$instance = &$this;
            add_action('init', array($this, 'st_checkout_packages'));
        }

        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public function st_checkout_packages()
        {
            if (STInput::post('action', '') === 'st_checkout_package') {
                $payment_gateway_id   = STInput::post('st_payment_gateway', 'st_submit_form');
                $payment_gateway_used = $this->get_gateway($payment_gateway_id);

                //=== Check Payment gateway
                if (!$payment_gateway_id || !$payment_gateway_used) {
                    $payment_gateway_name = apply_filters('st_payment_gateway_' . $payment_gateway_id . '_name', $payment_gateway_id);
                    STTemplate::set_mesage(sprintf(__('Sorry! Payment Gateway: <code>%s</code> is not available for this item!', ST_TEXTDOMAIN), $payment_gateway_name), 'danger');
                    return false;
                }
                //=== Check cart
                $cls_packages = STAdminPackages::get_inst();
                $cart         = $cls_packages->get_cart();
                if (!$cart) {
                    STTemplate::set_message(__('Your cart is currently empty', ST_TEXTDOMAIN), 'danger');
                    return false;
                }
                //=== Check Captcha
                if (st()->get_option('booking_enable_captcha', 'on') == 'on') {

                    $st_security_key = STInput::request('st_security_key');
                    $allow_captcha   = STInput::request('allow_capcha', 'off');
                    if ($allow_captcha == 'off') {
                        if (!$st_security_key) {
                            STTemplate::set_message(__('You did not enter the captcha', ST_TEXTDOMAIN), 'danger');
                            return false;
                        }
                        $valid = STCoolCaptcha::validate_captcha($st_security_key);
                        if (!$valid) {
                            STTemplate::set_message(__('Captcha is not correct', ST_TEXTDOMAIN), 'danger');
                            return false;
                        }
                    }

                }
                //=== Term and Condition
                if ((int) STInput::post('term_condition', '') != 1) {
                    STTemplate::set_message(__('Please accept our terms and conditions', ST_TEXTDOMAIN), 'danger');
                    return false;
                }

                //=== Save data
                global $wpdb;
                $table = $wpdb->prefix . 'st_member_packages_order';
                $cart  = $cls_packages->get_cart();

                $current_user = wp_get_current_user();

                $partner_info = array(
                    'firstname' => esc_html(STInput::post('st_first_name','') ),
                    'lastname' => esc_html(STInput::post('st_last_name','') ),
                    'email' => esc_html(STInput::post('st_email',$current_user->email) ),
                    'phone' => esc_html(STInput::post('st_phone', '') ),
                );
                $data  = array(
                    'package_id'            => $cart->id,
                    'package_name'          => $cart->package_name,
                    'package_price'         => $cart->package_price,
                    'package_time'          => $cart->package_time,
                    'package_commission'    => $cart->package_commission,
                    'package_item_upload'   => $cart->package_item_upload,
                    'package_item_featured' => $cart->package_item_featured,
                    'package_description'   => $cart->package_description,
                    'package_subname'       => $cart->package_subname,
                    'created'               => time(),
                    'partner'               => get_current_user_id(),
                    'status'                => 'pending',
                    'gateway'               => $payment_gateway_id,
                    'partner_info'          => serialize($partner_info)
                );
                $wpdb->insert($table, $data);
                $order_id = $wpdb->insert_id;

                $wpdb->update($table, array('token' => wp_hash($order_id)), array('id' => $order_id));

                $respon = $cls_packages->complete_purchase($payment_gateway_id, $order_id);

                if ( TravelHelper::st_compare_encrypt($order_id . 'st1', $respon['status'])) {
                    //=== Destroy cart session before redirect to payment
                    $cls_packages->destroy_cart();

                    //=== Incomplete.
                    $cls_packages->update_status('incomplete', $order_id);

                    if (!empty($respon['redirect_url'])) {
                        wp_redirect($respon['redirect_url']);
                        exit();
                    }
                    if (!empty($respon['redirect_form'])) {
                        echo $respon['redirect_form'];
                        exit();
                    }
                } elseif (TravelHelper::st_compare_encrypt($order_id . 'st0', $respon['status'])) {
                    STTemplate::set_message(sprintf(__('Your order have created but we can not process. Error Code: %s', ST_TEXTDOMAIN), $respon['message']), 'danger');
                    return false;
                }
            }
        }

        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public function get_gateway($payment_gateway_id)
        {
            $all = STPaymentGateways::get_payment_gateways();
            if (isset($all[$payment_gateway_id])) {
                $value = $all[$payment_gateway_id];
                if (method_exists($value, 'is_available')) {
                    return $value;
                }
            }
            return false;
        }

        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public function get_order_by_token($token)
        {
            $token = esc_sql($token);

            global $wpdb;
            $table = $wpdb->prefix . 'st_member_packages_order';
            $sql   = "SELECT * FROM {$table} WHERE token = '{$token}' LIMIT 1";
            return $wpdb->get_row($sql);
        }

        public function get_order_package_by($where = ''){

            global $wpdb;
            $table = $wpdb->prefix . 'st_member_packages_order';
            $sql   = "SELECT * FROM {$table} WHERE 1=1 AND {$where} LIMIT 1";
            return $wpdb->get_row($sql);
        }

        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public function convert_payment($payment){
            switch ($payment) {
                case 'st_submit_form':
                    $payment = __('Bank Transfer', ST_TEXTDOMAIN);
                    break;
                
                case 'st_paypal':
                    $payment = __('Paypal Express', ST_TEXTDOMAIN);
                    break;
                case 'st_payfast':
                    $payment = __('PayFast', ST_TEXTDOMAIN);
                    break;
                case 'st_stripe':
                    $payment = __('Stripe', ST_TEXTDOMAIN);
                    break;
            }

            return $payment;
        }

        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public function update_order($token = '', $status =''){
            global $pack_orderdata;
            $get_order_by_token = $this->get_order_by_token($token);
            if( $get_order_by_token && $get_order_by_token->gateway != 'st_submit_form'){
                $admin_packages = STAdminPackages::get_inst();
                if( TravelHelper::st_compare_encrypt((int)$get_order_by_token->id . 'st1', $status)){ //=== Completed
                    $admin_packages->update_status('completed', (int)$get_order_by_token->id);
                    
                }elseif(TravelHelper::st_compare_encrypt((int)$get_order_by_token->id . 'st0', $status)){ //=== Cancelled
                    $admin_packages->update_status('cancelled', (int)$get_order_by_token->id);
                }
            }

            //==== Send email
            $pack_orderdata = $get_order_by_token;
            $this->send_email($get_order_by_token);

            do_action('st_after_send_email_package', $get_order_by_token);

            return $get_order_by_token;
        }
        public function send_email($orderdata){
            $this->_send_admin($orderdata);
            $this->_send_customer($orderdata);
        }

        public function _send_admin($orderdata){

            $message = st()->load_template('email/header');
            $membership_email_admin = st()->get_option('membership_email_admin', '');
            $message .= do_shortcode($membership_email_admin);
            $message .= st()->load_template('email/footer');
            $subject = __('Have new a member package', ST_TEXTDOMAIN);

            $admin_email = st()->get_option('email_admin_address');

            $this->_send_mail($admin_email, $subject, $message);

        }
        public function _send_customer($orderdata){
            $message = st()->load_template('email/header');
            $membership_email_partner = st()->get_option('membership_email_partner', '');
            $message .= do_shortcode($membership_email_partner);
            $message .= st()->load_template('email/footer');

            $subject = __('You have registed a menber package', ST_TEXTDOMAIN);

            $partner_info = unserialize($orderdata->partner_info);
            $email = $partner_info['email'];

            $this->_send_mail($email, $subject, $message);
        }

        public function _send_mail($to, $subject, $message, $attachment=false){

            $from = st()->get_option('email_from');
            $from_address = st()->get_option('email_from_address');
            $headers = array();

            if($from and $from_address){
                $headers[]='From:'. $from .' <'.$from_address.'>';
            }

            add_filter( 'wp_mail_content_type', array($this,'set_html_content_type') );

            @wp_mail( $to, $subject, $message,$headers ,$attachment);
            
            remove_filter( 'wp_mail_content_type', array($this,'set_html_content_type') );
        }
        public function set_html_content_type() {

            return 'text/html';
        }
        /**
         *@since 1.3.1
         *@updated 1.3.1
         **/
        public static function get_inst()
        {
            return self::$instance;
        }
    }
    new STPackages();
}
