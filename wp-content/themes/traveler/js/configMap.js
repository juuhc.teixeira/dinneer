var map;
document.addEventListener('DOMContentLoaded', function() {
  var ulComponent = document.getElementsByClassName('nav-tabs');
  var googleMapsFrame = ulComponent[0].querySelectorAll("li")[1];

  console.log("Google maps frame");
  console.log(googleMapsFrame);

  googleMapsFrame.addEventListener("click", function() {
    console.log("Aba do mapa foi clicada");
    var returnDocument = document.getElementsByClassName("hidden st_detailed_map");
    var getLatLongInfo = returnDocument[0].getAttribute("data-data_show");

    //console.log(returnDocument);
    initialize();
    organizeDoc(getLatLongInfo);
  });

}, false);

function initialize() {
    var latlng = new google.maps.LatLng(0,0); //-20.4435, -54.6478
    var options = {
        zoom: 2,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapa"), options);
}

function organizeDoc(param){
  var str = JSON.stringify(param);
  var startLat = str.search("lat");
  var endLat = str.search("lng");
  var startLng = str.search("lng");
  var endLng = str.search("icon_mk");
  var lat = str.substring(startLat + 8, endLat - 5);
  var lng = str.substring(startLng + 8, endLng - 5);
  //console.log(lat + " "  + lng);
  var center = new google.maps.LatLng(lat, lng);

  // var marker = new google.maps.Marker({
  //     position: center,
  //     map: map,
  //     title: "Hello World"
  // });
  google.maps.event.trigger(map, 'resize');
  map.setCenter(center);
  map.setZoom(15);
  var circle = new google.maps.Circle({
      center: center,
      map: map,
      radius: 750,          // IN METERS.
      fillColor: '#f16667',
      fillOpacity: 0.3,
      strokeColor: "#f55c5d",
      strokeWeight: 1         // DON'T SHOW CIRCLE BORDER.
  });

  // var cityCircle = new google.maps.Circle({
  //     strokeColor: '#FF0000',
  //     strokeOpacity: 0.8,
  //     strokeWeight: 2,
  //     fillColor: '#FF0000',
  //     fillOpacity: 0.35,
  //     map: map,
  //     center: circleLatLng,
  //     radius: 1000
  //   });
}

document.addEventListener('visibilitychange', function(){
  console.log(document.activeElement);
})
