var arrayMarkers = {};
var ic_normal = 'http://development.dinneer.com/wp-content/themes/traveler/img/ic_map_normal.png';
var ic_selected = 'http://development.dinneer.com/wp-content/themes/traveler/img/ic_map_selected.png';
var keys;
var jsonObject = [];
var city;
var geocoder;
var req = new XMLHttpRequest();
var reqPost = new XMLHttpRequest();

document.addEventListener('DOMContentLoaded', function() {
    var map;
    initialize();
}, false);

function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(0, 0); //-20.4435, -54.6478
    var options = {
        zoom: 2,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,

    };
    map = new google.maps.Map(document.getElementById("mapa"), options);
    city = getParameterByName("city", window.location.href);
    centerCity(city);
    requests("?city=" +city);
}

function requests(param){
  var url = "http://development.dinneer.com/wp-content/themes/traveler/st_templates/user/getInfo.php";
  req.open("GET", url + param, true);
  req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  req.responseType = 'json';
  req.onload = function pinMap() {
      var result = JSON.stringify(req.response);
      jsonObject = JSON.parse(result);
      pinMapa(jsonObject);
  }
  req.send(null);
}

function callOnKey(params) {
    if (event.keyCode == 13) {
        newcidade = "?city=" + params.value;
        //window.location.search += '&city='+city;
        newURL(newcidade);
    }
}

function pinMapa(json) {
    console.log("json");
    console.log(json);
    keys = Object.keys(json);
    var tam = keys.length;
    var latlng = [];
    for (var i = 0; i < tam; i++) {
      for (var j = 0; j < json[keys[i]].length; j++) {
        if (j == 0) {
          lat = json[keys[i]][j].lat;
        } else {
          lng = json[keys[i]][j].lng;
        }
      }
      var newLatLng = new google.maps.LatLng(lat, lng);
      var title = keys[i];
      var marker = new google.maps.Marker({
          icon: ic_normal,
          position: newLatLng,
          map: map,
          title: keys[i]
      });
      arrayMarkers[title] = {
        "marker": marker
      };
    }
    //console.log(arrayMarkers);

}
function diselectPost(parametro) {
  //console.log(parametro.querySelector("#post_id").value);
  var currentPost = parametro.querySelector("#post_id").value;
  //console.log(arrayMarkers[currentPost]);
  var marker = arrayMarkers[currentPost].marker;
  marker.setIcon(ic_normal);
}

function selectPost(parametro) {
  //console.log(parametro.querySelector("#post_id").value);
  var currentPost = parametro.querySelector("#post_id").value;
  //console.log(arrayMarkers[currentPost]);
  var marker = arrayMarkers[currentPost].marker;
  marker.setIcon(ic_selected);
}

function newURL(newCity){
  var newurl = location.protocol + '//' + location.host + location.pathname;
  newurl = newurl + newCity;
  window.location.href = newurl;
  console.log(newurl);
}

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function centerCity(cidade) {
    geocoder.geocode({
        'address': cidade
    }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            map.setZoom(12);
            map.setCenter(results[0].geometry.location);
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}
