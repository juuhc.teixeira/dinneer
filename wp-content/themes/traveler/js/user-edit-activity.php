<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * User create activity
 *
 * Created by ShineTheme
 *
 */
wp_enqueue_script('bootstrap-timepicker.js');
wp_enqueue_script('bootstrap-datepicker.js');
wp_enqueue_script('bootstrap-datepicker-lang.js');

/* if( STUser_f::st_check_edit_partner(STInput::request('id')) == false ){
  return false;
  } */
$current_user = wp_get_current_user();
$post_id = STInput::request('id');
$title = $content = $excerpt = "";
if (!empty($post_id)) {
    $post = get_post($post_id);
    $title = $post->post_title;
    $content = $post->post_content;
    $excerpt = $post->post_excerpt;
}
$validator = STUser_f::$validator;
?>
<link href="<?php echo get_bloginfo('template_url'); ?>/css/common.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo get_bloginfo('template_url'); ?>/css/common-2.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo get_bloginfo('template_url'); ?>/css/common-3.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo get_bloginfo('template_url');?>/css/list_your_dinner.css" media="screen" rel="stylesheet" type="text/css"/>

<main id="site-content" role="main">
    <div class="container-fluid">
        <div class="row-fluid">
            <section id="form-part-1">
                <header>
                    <div class="list-your-space__header">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>

                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="space-5">
                                        <div class="landing__title-title">
                                            <span>
                                                <?php if (!empty($post_id)) { ?>
                                                <h2>Oi, <?php echo esc_html($current_user->display_name) ?>! <?php the_field('headline_nome_-_editar_anuncio'); ?></h2>
                                                <style>
                                                    .user-content.col-md-9 {
                                                        width: 73%;
                                                    }
                                                    .col-md-4.hidden-xs.hidden-sm {
                                                        display: none;
                                                    }
                                                    .list-your-space__header {
                                                        display: none;
                                                    }
                                                    section {
                                                        height: 100vh;
                                                        width: 100%;
                                                    }
                                                    section .col-md-6 {
                                                        width: 100%;
                                                        margin-top: 5%;
                                                    }
                                                    section .col-md-8 {
                                                        width: 100%;
                                                        box-shadow: none;
                                                    }
                                                    h2.landing__title-title {
                                                        display: none;
                                                    }
                                                    a.btn-progress-back {
                                                        display: none;
                                                    }
                                                    .btn-progress-next {
                                                        max-width: 50%;
                                                        display: none;
                                                    }
                                                </style>
                                                <?php } else { ?>

                                                    <?php
                                                        $image = get_field('imagem_part_1');

                                                        if( !empty($image) ): ?>
                                                        <style>
                                                            .landing__animation.hide-sm.lys-vertical-align-middle-container#part-1 {
                                                                background: url(<?php echo $image['url']; ?> ) no-repeat;
                                                                background-size: cover;
                                                                background-position: 50% 50%;
                                                                margin-top: 4%;
                                                            }
                                                        </style>


                                                        <?php endif; ?>
                                                    <style>
                                                        .container.bg-partner-new.create-activity {
                                                            padding: 0;
                                                            margin: 0;
                                                            width: 100% !important;
                                                        }
                                                        .user-content.col-md-9 {
                                                            width: 100%;
                                                        }
                                                        .col-md-4.landing__right-col {
                                                            background-color: <?php the_field('cor_background_right');?>;
                                                            padding: 0 3%;
                                                            margin-top: -11%;
                                                        }

                                                        .user-content.col-md-9 {
                                                            width: 100%;
                                                        }
                                                        div#st_header_wrap {
                                                            display: none;
                                                        }

                                                        .col-md-3.user-left-menu {
                                                            display: none;
                                                        }

                                                        .st-page-bar {
                                                            display: none;
                                                        }

                                                        div#mceu_31 {
                                                        height: 13vh;
                                                    }
                                                        body {
                                                            overflow: hidden;
                                                        }

                                                        #wpadminbar {
                                                            display: none !important;
                                                        }

                                                        .list-your-space__header img {
                                                            margin-top: -5px;
                                                        }

                                                        p.wcaiocc-text {
                                                            margin-top: -31%;
                                                        }
                                                    </style>
                                                    <h3>Oi, <?php echo esc_html($current_user->display_name) ?>! <?php the_field('headline_nome_-_novo_anuncio'); ?></h3>
                                                <?php } ?>

                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="msg">
                                        <?php echo STTemplate::message() ?>
                                        <?php echo STUser_f::get_msg(); ?>
                                        <?php echo STUser_f::get_control_data(); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <strong class="landing__step-number space-1 text-base text-branding text-light-gray">
                                        <span>Etapa 1</span>
                                    </strong>
                                    <div class="h3 landing__step-content-title space-3">
                                        <?php
                                        while (have_posts()) {
                                            the_post();
                                            ?>
                                            <span><?php the_field('texto_etapa_1'); ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <form class="earning-estimation-container" action="" method="post" enctype="multipart/form-data" id="st_form_add_partner">
                                    <?php if (!empty($post_id)) { ?>
                                        <?php wp_nonce_field('user_setting', 'st_update_post_activity'); ?>
                                    <?php } else { ?>
                                    <?php wp_nonce_field('user_setting', 'st_insert_post_activity'); ?><?php } ?>
                                    <div class="form-group form-group-icon-left">
                                        <div class="form-control">
                                            <div class="row row-condensed">
                                                <div class="col-sm-12" data-reactid="54">
                                                    <i class="fa  fa-file-text input-icon input-icon-hightlight"></i>
                                                    <input id="title" name="st_title" type="text" placeholder="<?php echo __('Name of activity', ST_TEXTDOMAIN); ?>" class="list-space-location-input pull-left" value="<?php echo STInput::request("st_title", $title) ?>">
                                                    <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('st_title'), 'danger') ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $taxonomies = (get_object_taxonomies('st_activity'));
                                    if (is_array($taxonomies) and ! empty($taxonomies)) {
                                        foreach ($taxonomies as $key => $value) {
                                            ?>
                                            <div class="col-md-12">
                                                <?php
                                                $category = STUser_f::get_list_taxonomy($value);
                                                $taxonomy_tmp = get_taxonomy($value);
                                                $taxonomy_label = ($taxonomy_tmp->label );
                                                $taxonomy_name = ($taxonomy_tmp->name );

                                                if (!empty($category) & ($taxonomy_name == "meal")):
                                                    ?>
                                                    <div class="form-group form-group-icon-left">
                                                        <label for="check_all"> <?php echo esc_html($taxonomy_label); ?>:</label>
                                                        <div class="row">
                                                            <?php
                                                            foreach ($category as $k => $v):
                                                                $icon = get_tax_meta($k, 'st_icon');
                                                                $icon = TravelHelper::handle_icon($icon);
                                                                $check = '';
                                                                if (STUser_f::st_check_post_term_partner($post_id, $value, $k) == true) {
                                                                    $check = 'checked';
                                                                }
                                                            ?>
                                                                <div class="col-md-4">
                                                                    <div class="checkbox-inline checkbox-stroke ">
                                                                        <label for="taxonomy">
                                                                            <i class="<?php echo esc_html($icon) ?>"></i>

                                                                            <input name="taxonomy[]" class="i-check item_tanoxomy"  <?php echo esc_html($check) ?> type="checkbox" value="<?php echo esc_attr($k . ',' . $taxonomy_name) ?>" /><?php echo esc_html($v) ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input name="no_taxonomy" type="hidden" value="no_taxonomy">
                                    <?php } ?>
                                        <?php
                                    $taxonomies = (get_object_taxonomies('st_activity'));
                                    if (is_array($taxonomies) and ! empty($taxonomies)) {
                                        foreach ($taxonomies as $key => $value) {
                                            ?>
                                            <div class="col-md-12">
                                                <?php
                                                $category = STUser_f::get_list_taxonomy($value);
                                                $taxonomy_tmp = get_taxonomy($value);
                                                $taxonomy_label = ($taxonomy_tmp->label );
                                                $taxonomy_name = ($taxonomy_tmp->name );

                                                if (!empty($category) & ($taxonomy_name == "disponibilidade")):
                                                    ?>
                                                    <div class="form-group form-group-icon-left">
                                                        <label for="check_all"> <?php echo esc_html($taxonomy_label); ?>:</label>
                                                        <div class="row">
                                                            <?php
                                                            foreach ($category as $k => $v):
                                                                $icon = get_tax_meta($k, 'st_icon');
                                                                $icon = TravelHelper::handle_icon($icon);
                                                                $check = '';
                                                                if (STUser_f::st_check_post_term_partner($post_id, $value, $k) == true) {
                                                                    $check = 'checked';
                                                                }
                                                            ?>
                                                                <div class="col-md-4">
                                                                    <div class="checkbox-inline checkbox-stroke ">
                                                                        <label for="taxonomy">
                                                                            <i class="<?php echo esc_html($icon) ?>"></i>

                                                                            <input name="taxonomy[]" class="i-check item_tanoxomy"  <?php echo esc_html($check) ?> type="checkbox" value="<?php echo esc_attr($k . ',' . $taxonomy_name) ?>" /><?php echo esc_html($v) ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input name="no_taxonomy" type="hidden" value="no_taxonomy">
                                    <?php } ?>
                                </div>

                                <footer>
                                    <div class="bg-white main-panel-outer-half clearfix">
                                        <div class="no-margin-padding__sm main-panel-padding main-panel-progress pull-right main-panel-inner-half space-sm-8">
                                            <div class="divider hide-sm"></div>
                                            <div class="row no-margin-padding__sm">
                                                <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                                    <div>
                                                        <a class="btn btn-large btn-progress-next btn-large__next-btn pull-right-md btn-primary" to="#form-part-2" href="#form-part-2">
                                                            <div class="btn-progress-next__text">
                                                                <span>Continuar</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </div>
                            <div class="col-md-6 landing__right-col hidden-sm hidden-xs">
                                <div>
                                    <div class="landing__animation hide-sm lys-vertical-align-middle-container" id="part-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <section id="form-part-2">
                <header>
                    <div class="list-your-space__header" style="position: absolute; width: 100vw;">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>
                                <div class="comp comp__step-bar-wrapper no-hover pull-left hide-sm"><span class="hdr-btn"><span class="hide-sm h5 text-normal">
                                        </span><span class="h5 text-normal"><?php the_field('titulo_etapa_1'); ?></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                        <div class="col-md-8">
                            <div class="space-5">
                                <h2 class="landing__title-title">
                                    <span data-reactid="18">
                                        <?php the_field('headline_etapa_1'); ?>
                                    </span>
                                </h2>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-icon-left" id="description">
                                        <label for="st_content" class="head_bol"><?php _e("Description", ST_TEXTDOMAIN) ?>:</label>
                                        <textarea id="st_content" rows="2" name="st_content" class="form-control"><?php echo STInput::request('st_content', get_post_meta($post_id, 'st_content', true)) ?></textarea>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('st_content'), 'danger') ?></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="desc" class="head_bol"><?php _e("Entre", ST_TEXTDOMAIN) ?>:</label>
                                        <textarea id="st_entre" rows="2" name="st_entre" class="form-control"><?php echo STInput::request('st_entre', get_post_meta($post_id, 'st_entre', true)) ?></textarea>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('st_entre'), 'danger') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="st_maincourse" class="head_bol"><?php _e("Main course", ST_TEXTDOMAIN) ?>:</label>
                                        <textarea id="st_maincourse" rows="2" name="st_maincourse" class="form-control"><?php echo STInput::request('st_maincourse', get_post_meta($post_id, 'st_maincourse', true)) ?></textarea>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('maincourse'), 'danger') ?></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dessert" class="head_bol"><?php _e("Dessert", ST_TEXTDOMAIN) ?>:</label>
                                        <textarea id="dessert" rows="2" name="st_dessert" class="form-control"><?php echo STInput::request('st_dessert', get_post_meta($post_id, 'st_dessert', true)) ?></textarea>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('st_dessert'), 'danger') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="drinks" class="head_bol"><?php _e("Drinks", ST_TEXTDOMAIN) ?>:</label>
                                        <textarea id="drinks" rows="2" name="st_drinks" class="form-control"><?php echo STInput::request('st_drinks', get_post_meta($post_id, 'st_drinks', true)) ?></textarea>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('st_drinks'), 'danger') ?></div>
                                    </div>
                                </div>
                            </div>
                            <footer>
                                <div class="bg-white main-panel-outer-half clearfix">
                                    <div class="no-margin-padding__sm main-panel-padding main-panel-progress main-panel-inner-half space-sm-8">
                                        <div class="divider hide-sm"></div>
                                        <div class="row no-margin-padding__sm">
                                            <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                                <div>
                                                    <a class="btn-progress-back link-icon va-container va-container-v pull-left text-gray link--accessibility-outline" to="#form-part-1" href="#form-part-1">
                                                        <span class="icon hide-sm"></span>
                                                        <span class="va-middle">
                                                            <h5 class="text-normal">
                                                                <span>Voltar</span>
                                                            </h5>

                                                        </span>
                                                    </a>
                                                    <a class="btn btn-large btn-progress-next btn-large__next-btn pull-right-md btn-primary" to="#form-part-3" href="#form-part-3">
                                                        <div class="btn-progress-next__text">
                                                            <span>Continuar</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                        <div class="col-md-4 hidden-xs hidden-sm">
                            <div>
                                <div class="landing__animation hide-sm lys-vertical-align-middle-container">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('icon_dica_1');?></span>
                                            <?php the_field('dica_part_1'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </article>
            </section>
            <section id="form-part-3">
                <header>
                    <div class="list-your-space__header">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>
                                <div class="comp comp__step-bar-wrapper no-hover pull-left hide-sm"><span class="hdr-btn"><span class="hide-sm h5 text-normal">
                                        </span><span class="h5 text-normal"><?php the_field('titulo_etapa_1'); ?></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="space-5">
                                    <h2 class="landing__title-title">
                                        <span data-reactid="18">
                                            <?php the_field('headline_etapa_3'); ?>
                                        </span>
                                    </h2>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group" >
                                                <label for="max_people"><?php _e("Group of", ST_TEXTDOMAIN) ?>:</label>
                                                <input id="max_people" name="max_people" type="text" min="0" placeholder="<?php _e("Max people", ST_TEXTDOMAIN) ?>" class="form-control number" value="<?php echo STInput::request('max_people', get_post_meta($post_id, 'max_people', true)) ?>">
                                            </div>
                                            <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('max_people'), 'danger') ?></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-group-icon-left">

                                                <label for="adult_price"><?php _e("Price", ST_TEXTDOMAIN) ?>:</label>
                                                <i class="fa fa-money input-icon input-icon-hightlight"></i>
                                                <input id="adult_price" name="adult_price" type="text"
                                                       placeholder="<?php _e("Price", ST_TEXTDOMAIN) ?>" class="form-control number" value="<?php echo STInput::request('adult_price', get_post_meta($post_id, 'adult_price', true)) ?>">
                                                <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('adult_price'), 'danger') ?></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group form-group-icon-left">

                                                <label for="currency"><?php _e("Currency", ST_TEXTDOMAIN) ?>:</label>
                                                <i class="fa fa-money input-icon input-icon-hightlight"></i>
                                                <input id="currency" name="currency" type="text" placeholder="<?php _e("Currency", ST_TEXTDOMAIN) ?>" class="form-control" value="<?php echo STInput::request('currency', get_post_meta($post_id, 'currency', true)) ?>">
                                                <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('currency'), 'danger') ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <?php echo st()->load_template('user/tabs/cancel-booking', FALSE, array('validator' => $validator)) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" style="display:none;">
                                        <div class="form-group form-group-icon-left">
                                            <label for="type_activity"><?php _e("Select activity type", ST_TEXTDOMAIN) ?>:</label>
                                            <i class="fa fa-calendar input-icon input-icon-hightlight"></i>
        <?php $type_activity = STInput::request('type_activity', get_post_meta($post_id, 'type_activity', true)); ?>
                                            <select class="form-control" name="type_activity" id="type_activity">
                                                <option selected="selected" value="daily_activity"><?php _e('Daily Activity', ST_TEXTDOMAIN) ?></option>
                                                <option  value="specific_date"><?php _e('Specific Date', ST_TEXTDOMAIN) ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 data_duration">
                                        <div class="form-group form-group-icon-left">
                                            <label for="duration"><?php st_the_language('user_create_activity_duration') ?>:</label>
                                            <i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                            <input type="text" name="duration" id="duration" class="time-pick form-control" placeholder="<?php st_the_language('user_create_activity_duration') ?>" value="<?php echo STInput::request('duration', get_post_meta($post_id, 'duration', true)) ?>">
                                        </div>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('duration'), 'danger') ?></div>
                                    </div>
                                    <div class="col-md-6 data_duration">
                                        <div class="form-group form-group-icon-left">
                                            <label for="activity-time"><?php st_the_language('user_create_activity_activity_time') ?>:</label>
                                            <i class="fa fa-clock-o input-icon input-icon-hightlight"></i>
                                            <input name="activity-time" id="activity-time" class="time-pick form-control" type="text" placeholder="<?php st_the_language( 'user_create_activity_activity_time' ) ?>" value="<?php echo STInput::request('activity-time', get_post_meta($post_id, 'activity-time', true)) ?>">
                                        </div>
                                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('activity-time'), 'danger') ?></div>
                                    </div>
                                    <div class="bg-white main-panel-outer-half clearfix">
                                        <div class="no-margin-padding__sm main-panel-padding main-panel-progress pull-right main-panel-inner-half space-sm-8">
                                            <div class="divider hide-sm"></div>
                                            <div class="row no-margin-padding__sm">
                                                <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                                    <div>
                                                        <a class="btn-progress-back link-icon va-container va-container-v pull-left text-gray link--accessibility-outline" to="#form-part-2" href="#form-part-2">
                                                            <span class="icon hide-sm"></span>
                                                            <span class="va-middle">
                                                                <h5 class="text-normal">
                                                                    <span>Voltar</span>
                                                                </h5>

                                                            </span>
                                                        </a>
                                                        <a class="btn btn-large btn-progress-next btn-large__next-btn pull-right-md btn-primary" to="#form-part-4" href="#form-part-4">
                                                            <div class="btn-progress-next__text">
                                                                <span>Continuar</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-xs hidden-sm">
                                <div>
                                    <div class="landing__animation hide-sm lys-vertical-align-middle-container">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('icon_dica_3');?></span>
                                                <?php the_field('dica_part_3'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <section id="form-part-4">
                <header>
                    <div class="list-your-space__header">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>
                                <div class="comp comp__step-bar-wrapper no-hover pull-left hide-sm"><span class="hdr-btn"><span class="hide-sm h5 text-normal">
                                        </span><span class="h5 text-normal"><?php the_field('titulo_etapa_1'); ?></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="space-5">
                                    <h2 class="landing__title-title">
                                        <span data-reactid="18">
                                            <?php the_field('headline_etapa_4'); ?>
                                        </span>
                                    </h2>
                                </div>
                                <div class="row" style="margin-top: -4%;">
                            <div class="col-md-12">
                                <div class="form-group form-group-icon-left">
                                    <label class="head_bol"><?php _e("Featured image", ST_TEXTDOMAIN) ?>:</label>
                                    <?php
                                        if (!empty($post_id)) {
                                            $id_img = get_post_thumbnail_id($post_id);
                                        } else {
                                            $id_img = STInput::request('id_featured_image');
                                        }
                                        $post_thumbnail_id = wp_get_attachment_image_src($id_img, 'full');
                                    ?>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                <?php _e("Browse…", ST_TEXTDOMAIN) ?> <input name="featured-image"  type="file" >
                                            </span>
                                        </span>
                                        <input type="text" readonly="" value="<?php echo esc_url($post_thumbnail_id['0']); ?>" class="form-control data_lable">
                                    </div>
                                    <input id="id_featured_image" name="id_featured_image" type="hidden" value="<?php echo esc_attr($id_img) ?>">
                                    <?php
                                        if (!empty($post_thumbnail_id)) {
                                            echo '<div class="user-profile-avatar user_seting st_edit">
                                        <div><img width="300" height="300" class="avatar avatar-300 photo img-thumbnail" src="' . $post_thumbnail_id['0'] . '" alt=""></div>
                                        <input name="" type="button"  class="btn btn-danger  btn_featured_image" value="' . st_get_language('user_delete') . '">
                                      </div>';
                                        }
                                    ?>
                                    <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('featured_image'), 'danger') ?></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                        <div class="form-group form-group-icon-left">
                            <label for="id_gallery"><?php _e( "Gallery" , ST_TEXTDOMAIN ) ?>:</label>
                            <?php
                            if(!empty($post_id)){
                                $id_img = get_post_meta($post_id , 'gallery',true);
                            }else{
                                $id_img = STInput::request('id_gallery');
                            }
                            ?>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-primary btn-file">
                                        <?php _e( "Browse…" , ST_TEXTDOMAIN ) ?> <input name="gallery[]" id="gallery" multiple type="file">
                                    </span>
                                </span>
                                <input type="text" readonly="" value="<?php echo esc_html( $id_img ) ?>"
                                       class="form-control data_lable">
                            </div>
                            <input id="id_gallery" name="id_gallery" type="hidden" value="<?php echo esc_attr( $id_img ) ?>">
                            <?php
                            if(!empty( $id_img )) {
                                echo '<div class="user-profile-avatar user_seting st_edit"><div>';
                                foreach( explode( ',' , $id_img ) as $k => $v ) {
                                    $post_thumbnail_id = wp_get_attachment_image_src( $v , 'full' );
                                    echo '<img width="300" height="300" class="avatar avatar-300 photo img-thumbnail" src="' . $post_thumbnail_id[ '0' ] . '" alt="">';
                                }
                                echo '</div><input name="" type="button"  class="btn btn-danger  btn_del_gallery" value="' . st_get_language( 'user_delete' ) . '"></div>';
                            }
                            ?>
                        </div>
                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('gallery'),'danger') ?></div>
                    </div>
<!--                            <div class="col-md-6">
                                <div class="form-group form-group-icon-left">

                                    <label for="video"><?php st_the_language('user_create_activity_video') ?>:</label>
                                    <i class="fa  fa-youtube-play input-icon input-icon-hightlight"></i>
                                    <input id="video" name="video" type="text"
                                           placeholder="<?php _e("Enter Youtube or Vimeo video link (Eg: https://www.youtube.com/watch?v=JL-pGPVQ1a8)") ?>" class="form-control" value="<?php echo STInput::request('video', get_post_meta($post_id, 'video', true)) ?>">
                                </div>
                                <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('video'), 'danger') ?></div>
                            </div>-->
                                    <div class="col-md-12">
                                        <?php
                                    $taxonomies = (get_object_taxonomies('st_activity'));
                                    if (is_array($taxonomies) and ! empty($taxonomies)) {
                                        foreach ($taxonomies as $key => $value) {
                                            ?>
                                            <div class="col-md-12">
                                                <?php
                                                $category = STUser_f::get_list_taxonomy($value);
                                                $taxonomy_tmp = get_taxonomy($value);
                                                $taxonomy_label = ($taxonomy_tmp->label );
                                                $taxonomy_name = ($taxonomy_tmp->name );

                                                if (!empty($category) & ($taxonomy_name == "tem_na_casa")):
                                                    ?>
                                                    <div class="form-group form-group-icon-left">
                                                        <label for="check_all"> <?php echo esc_html($taxonomy_label); ?>:</label>
                                                        <div class="row">
                                                            <?php
                                                            foreach ($category as $k => $v):
                                                                $icon = get_tax_meta($k, 'st_icon');
                                                                $icon = TravelHelper::handle_icon($icon);
                                                                $check = '';
                                                                if (STUser_f::st_check_post_term_partner($post_id, $value, $k) == true) {
                                                                    $check = 'checked';
                                                                }
                                                            ?>
                                                                <div class="col-md-4">
                                                                    <div class="checkbox-inline checkbox-stroke ">
                                                                        <label for="taxonomy">
                                                                            <i class="<?php echo esc_html($icon) ?>"></i>

                                                                            <input name="taxonomy[]" class="i-check item_tanoxomy"  <?php echo esc_html($check) ?> type="checkbox" value="<?php echo esc_attr($k . ',' . $taxonomy_name) ?>" /><?php echo esc_html($v) ?>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <input name="no_taxonomy" type="hidden" value="no_taxonomy">
                                    <?php } ?>
                                    </div>
                            <div class="bg-white main-panel-outer-half clearfix">
                                <div class="no-margin-padding__sm main-panel-padding main-panel-progress pull-right main-panel-inner-half space-sm-8">
                                    <div class="divider hide-sm"></div>
                                    <div class="row no-margin-padding__sm">
                                        <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                            <div>
                                                <a class="btn-progress-back link-icon va-container va-container-v pull-left text-gray link--accessibility-outline" to="#form-part-3" href="#form-part-3">
                                                    <span class="icon hide-sm"></span>
                                                    <span class="va-middle">
                                                        <h5 class="text-normal">
                                                            <span>Voltar</span>
                                                        </h5>

                                                    </span>
                                                </a>
                                                <a class="btn btn-large btn-progress-next btn-large__next-btn pull-right-md btn-primary" to="#form-part-5" href="#form-part-5">
                                                    <div class="btn-progress-next__text">
                                                        <span>Continuar</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-xs hidden-sm">
                                <div>
                                    <div class="landing__animation hide-sm lys-vertical-align-middle-container">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('icon_dica_4');?></span>
                                                <?php the_field('dica_part_4'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </article>
            </section>
            <section id="form-part-5">
                <header>
                    <div class="list-your-space__header">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>
                                <div class="comp comp__step-bar-wrapper no-hover pull-left hide-sm"><span class="hdr-btn"><span class="hide-sm h5 text-normal">
                                        </span><span class="h5 text-normal"><?php the_field('titulo_etapa_1'); ?></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="space-5">
                                    <h2 class="landing__title-title">
                                        <span data-reactid="18">
                                            <?php the_field('headline_etapa_5'); ?>
                                        </span>
                                    </h2>
                                </div>
                                <div class="row" >
                                    <div id="overflow">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="venue-facilities"><?php st_the_language('user_create_activity_venue_facilities') ?>:</label>
                                                <textarea class="form-control" name="venue-facilities"><?php echo STInput::request('venue-facilities', get_post_meta($post_id, 'venue-facilities', true)) ?></textarea>
                                            </div>
                                            <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('venue-facilities'), 'danger') ?></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group form-group-icon-left">
                                                <label for="multi_location"><?php st_the_language( 'user_create_car_location' ) ?>:</label>
                                                <div id="setting_multi_location" class="location-front">
                                                    <?php
                                                        $html_location = TravelHelper::treeLocationHtml();
                                                        $post_id = STInput::request('id','');

                                                        $multi_location = get_post_meta( $post_id, 'multi_location', true );
                                                        if( !empty( $multi_location ) && !is_array( $multi_location ) ){
                                                            $multi_location = explode(',', $multi_location);
                                                        }
                                                        if( empty( $multi_location ) ){
                                                            $multi_location = array('');
                                                        }
                                                    ?>
                                <div class="form-group st-select-loction">
                                    <input placeholder="<?php echo __('Type to searchs', ST_TEXTDOMAIN); ?>" type="text" class="widefat form-control" name="search" value="">
                                    <div class="list-location-wrapper">
                                        <?php
                                            if(is_array($html_location) && count($html_location)):
                                                foreach($html_location as $key => $location):
                                        ?>
                                            <div data-name="<?php echo $location['parent_name']; ?>" class="item" style="margin-left: <?php echo $location['level'].'px;'; ?> margin-bottom: 5px;">
                                                <label for="<?php echo 'location-'.$location['ID']; ?>">
                                                    <input <?php if(in_array('_'.$location['ID'].'_', $multi_location)) echo 'checked'; ?>  id="<?php echo 'location-'.$location['ID']; ?>" type="checkbox" name="multi_location[]" value="<?php echo '_'.$location['ID'].'_'; ?>" checked>
                                                    <span><?php echo $location['post_title']; ?></span>
                                                </label>
                                            </div>
                                        <?php  endforeach; endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('multi_location'),'danger') ?></div>
                        </div>
                    </div>
                                        <div class="col-md-12">
                        <div class="form-group form-group-icon-left">
                            <script src="<?php echo get_bloginfo('template_url'); ?>/js/getLatLng.js"></script>
                            <label for="address">
                              <?php _e("Address",ST_TEXTDOMAIN) ?>:</label>
                            <i class="fa fa-home input-icon input-icon-hightlight"></i>
                            <input id="address" name="address" type="text"
                                   placeholder="
                                   <?php _e("Address",ST_TEXTDOMAIN) ?>"
                                   class="form-control" value="
                                   <?php echo STInput::request('address',get_post_meta($post_id,'address',true)) ?>">
                            <div class="st_msg">
                              <?php echo STUser_f::get_msg_html($validator->error('address'),'danger') ?>
                            </div>
                        </div>
                    </div>
                                         <div class="col-md-12 partner_map">
                        <?php
                        if(class_exists('BTCustomOT')){
                            BTCustomOT::load_fields();
                            ot_type_bt_gmap_html();
                        }
                        ?>
                    </div>
                    <div class="col-md-6">
                        <br>
                        <div class='form-group form-group-icon-left'>
                            <label for="is_featured"><?php _e( "Enable Street Views" , ST_TEXTDOMAIN ) ?>:</label>
                            <i class="fa fa-cogs input-icon input-icon-hightlight"></i>
                            <?php $enable_street_views_google_map  = STInput::request('enable_street_views_google_map',get_post_meta($post_id,'enable_street_views_google_map',true)) ?>
                            <select class='form-control' name='enable_street_views_google_map' id="enable_street_views_google_map">
                                <option value='on' <?php if($enable_street_views_google_map == 'on') echo 'selected'; ?> ><?php _e("On",ST_TEXTDOMAIN) ?></option>
                                <option value='off' <?php if($enable_street_views_google_map == 'off') echo 'selected'; ?> ><?php _e("Off",ST_TEXTDOMAIN) ?></option>
                            </select>
                        </div>
                    </div>
                                    </div>
                                    <div class="bg-white main-panel-outer-half clearfix">
                                <div class="no-margin-padding__sm main-panel-padding main-panel-progress pull-right main-panel-inner-half space-sm-8">
                                    <div class="divider hide-sm"></div>
                                    <div class="row no-margin-padding__sm">
                                        <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                            <div>
                                                <a class="btn-progress-back link-icon va-container va-container-v pull-left text-gray link--accessibility-outline" to="#form-part-4" href="#form-part-4">
                                                    <span class="icon hide-sm"></span>
                                                    <span class="va-middle">
                                                        <h5 class="text-normal">
                                                            <span>Voltar</span>
                                                        </h5>

                                                    </span>
                                                </a>
                                                <a class="btn btn-large btn-progress-next btn-large__next-btn pull-right-md btn-primary" to="#form-part-6" href="#form-part-6">
                                                    <div class="btn-progress-next__text">
                                                        <span>Continuar</span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-xs hidden-sm">
                            <div>
                                <div class="landing__animation hide-sm lys-vertical-align-middle-container">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('icon_dica_5');?></span>
                                            <?php the_field('dica_part_5'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </article>
            </section>
            <section id="form-part-6">
                <header>
                    <div class="list-your-space__header">
                        <div class="airbnb-header new">
                            <div class="regular-header clearfix">
                                <div class="comp pull-left"><a to="/" class="hdr-btn link-reset belo-container link--accessibility-outline" href="/"><img src="http://development.dinneer.com/wp-content/uploads/2017/03/dinneer-1.png" alt="Dinneer" style="width:150px"></a></div>
                                <div class="comp comp__step-bar-wrapper no-hover pull-left hide-sm"><span class="hdr-btn"><span class="hide-sm h5 text-normal">
                                        </span><span class="h5 text-normal"><?php the_field('titulo_etapa_1'); ?></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            </div>
                        </div>
                    </div>
                </header>
                <article>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="space-5">
                                    <h2 class="landing__title-title">
                                        <span data-reactid="18">
                                            <?php the_field('headline_etapa_6'); ?>
                                        </span>
                                    </h2>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-icon-left">
                                            <label for="email"><?php st_the_language('user_create_activity_email') ?>:</label>
                                            <i class="fa  fa-envelope-o input-icon input-icon-hightlight"></i>
                                            <input id="email" name="email" type="email" placeholder="<?php st_the_language('user_create_activity_email') ?>" class="form-control" value="<?php echo $current_user->user_email  ?>">
                                            <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('email'), 'danger') ?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-icon-left">

                                            <label for="phone"><?php st_the_language('user_create_activity_phone') ?>:</label>
                                            <i class="fa  fa-phone input-icon input-icon-hightlight"></i>
                                            <input id="phone" name="phone" type="phone" placeholder="<?php st_the_language('user_create_activity_phone') ?>" class="form-control" value="<?php echo STInput::request('contact_phone', get_post_meta($post_id, 'contact_phone', true)) ?>">
                                            <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('phone'), 'danger') ?></div>
                                        </div>
                                    </div>
                                    <div class="bg-white main-panel-outer-half clearfix">
                                        <div class="no-margin-padding__sm main-panel-padding main-panel-progress pull-right main-panel-inner-half space-sm-8">
                                            <div class="divider hide-sm"></div>
                                            <div class="row no-margin-padding__sm">
                                                <div class="main-panel__actions col-sm-12 no-margin-padding__sm">
                                                    <div>
                                                        <div class="text-center div_btn_submit">

                                                        </div>
                                                        <a class="btn-progress-back link-icon va-container va-container-v pull-left text-gray link--accessibility-outline" to="#form-part-5" href="#form-part-5">
                                                            <span class="icon hide-sm"></span>
                                                            <span class="va-middle">
                                                                <h5 class="text-normal">
                                                                    <span>Voltar</span>
                                                                </h5>

                                                            </span>
                                                        </a>
                                                        <div>
                                                            <?php if(!empty($post_id)){?>
                                                                <input type="button" id="btn_check_insert_activity" class="btn btn-primary btn-lg" value="<?php _e("UPDATE ACTIVITY",ST_TEXTDOMAIN) ?>">
                                                                <input name="btn_update_post_type_activity" id="btn_insert_post_type_activity" type="submit" class="btn btn-primary hidden btn_partner_submit_form" value="SUBMIT">
                                                            <?php }else{ ?>
                                                                <input  type="hidden"  class="save_and_preview" name="save_and_preview" value="false">
                                                                <input  type="hidden" id=""  class="" name="action_partner" value="add_partner">
                                                                <input name="btn_insert_post_type_activity" id="btn_insert_post_type_activity" type="submit" class="btn btn-primary btn-lg btn_partner_submit_form" value="<?php _e("SUBMIT ACTIVITY",ST_TEXTDOMAIN) ?>">
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 hidden-xs hidden-sm">
                                <div>
                                    <div class="landing__animation hide-sm lys-vertical-align-middle-container">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('icon_dica_6');?></span>
                                                <?php the_field('dica_part_6'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
