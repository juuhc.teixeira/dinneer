
<!DOCTYPE html>
<!--[if IE 9]>
  <html lang="pt"
      
      xmlns:fb="http://ogp.me/ns/fb#"
      class="ie ie9">
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt"

      xmlns:fb="http://ogp.me/ns/fb#">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <link href="<?php echo get_bloginfo('template_url'); ?>/css/common.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_bloginfo('template_url'); ?>/css/common-2.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_bloginfo('template_url'); ?>/css/common-3.css" media="screen" rel="stylesheet" type="text/css" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



        <meta id="csrf-param-meta-tag" name="csrf-param" content="authenticity_token"/>
        <meta id="csrf-token-meta-tag" name="csrf-token" content=""/>


        <title>Alugue o Seu Quarto, Casa ou Apartamento no Airbnb</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
<?php
    $post_id = STInput::request('id');
    $title = $content = $excerpt = "";
    if(!empty($post_id)){
        $post = get_post( $post_id );
        $title = $post->post_title;
        $content = $post->post_content;
        $excerpt = $post->post_excerpt;
    }
    $validator= STUser_f::$validator;
    
?> 
<?php if(!empty($post_id)){?>
            <?php _e("Edit Activity",ST_TEXTDOMAIN) ?>
        <?php }else{ ?>
            <?php _e("Add Activity",ST_TEXTDOMAIN) ?>
        <?php } ?>
        <main id="site-content" role="main">
            <div class="container">
                <div class="row">
<div class="msg">
    <?php echo STTemplate::message() ?>
    <?php echo STUser_f::get_msg(); ?>
    <?php echo STUser_f::get_control_data(); ?>
</div>
                    <div class="col-md-7 landing__left-col fast-animation" data-reactid="14">
                        <div class="landing__left-col-content" data-reactid="15">
                       <div class="space-5" data-reactid="16">
                           <h2 class="landing__title-title" data-reactid="17">
                               <span data-reactid="18">Oi, <?php echo esc_html($current_user->display_name) ?>! <?php the_field('headline_nome');?></span>
                           </h2>
                       </div>
                       <div class="landing__step-content landing__wmpw-controls" data-reactid="19">
                           <strong class="landing__step-number space-1 text-base text-branding text-light-gray" data-reactid="20">
                               <span data-reactid="21">Etapa 1</span>
                           </strong>
                           <div class="h3 landing__step-content-title space-3" data-reactid="22">
                               <span data-reactid="23"><?php the_field('texto_etapa_1');?></span>
                           </div>
                           <form action="" method="post" enctype="multipart/form-data" id="st_form_add_partner">
    <?php if(!empty($post_id)){?>
        <?php wp_nonce_field('user_setting','st_update_post_activity'); ?>
    <?php }else{ ?>
        <?php wp_nonce_field('user_setting','st_insert_post_activity'); ?>
    <?php } ?>
    <div class="row">
                    <?php
                    $taxonomies = (get_object_taxonomies('st_activity'));
                    if (is_array($taxonomies) and !empty($taxonomies)){
                        foreach ($taxonomies as $key => $value) {
                            ?>
                            <div class="col-md-12">
                                <?php
                                $category = STUser_f::get_list_taxonomy($value);
                                $taxonomy_tmp = get_taxonomy( $value );
                                $taxonomy_label =  ($taxonomy_tmp->label );
                                $taxonomy_name =  ($taxonomy_tmp->name );
                                                
                                if(!empty($category)):
                                    ?>
                                    <div class="form-group form-group-icon-left">
                                        <label for="check_all"> <?php echo esc_html($taxonomy_label); ?>:</label>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="checkbox-inline checkbox-stroke">
                                                    <label for="check_all">
                                                        <i class="fa fa-cogs"></i>
                                                        <input name="check_all" class="i-check check_all" type="checkbox"  /><?php _e("All",ST_TEXTDOMAIN) ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php foreach($category as $k=>$v):
                                                $icon = get_tax_meta($k,'st_icon');
                                                $icon = TravelHelper::handle_icon($icon);
                                                $check = '';
                                                if(STUser_f::st_check_post_term_partner( $post_id  ,$value , $k) == true ){
                                                    $check = 'checked';
                                                }
                                                ?>
                                                <div class="col-md-3">
                                                    <div class="checkbox-inline checkbox-stroke ">
                                                        <label for="taxonomy">
                                                            <i class="<?php echo esc_html($icon) ?>"></i>
                                                            
                                                            <input name="taxonomy[]" class="i-check item_tanoxomy"  <?php echo esc_html($check) ?> type="checkbox" value="<?php echo esc_attr($k.','.$taxonomy_name) ?>" /><?php echo esc_html($v) ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                        <?php
                        }
                    } else { ?>
                        <input name="no_taxonomy" type="hidden" value="no_taxonomy">
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="st_msg"><?php echo STUser_f::get_msg_html($validator->error('taxonomy[]'),'danger') ?></div>
                    </div>
                </div>
                           </form>
                           <!--
                           <form class="earning-estimation-container" data-prevent-default="true" data-reactid="24">
                               <div class="earning-estimation" data-reactid="25">
                                   <div class="earning-estimation__body" data-reactid="26">
                                       <div class="row row-condensed" data-reactid="27">
                                           <div class="col-md-6 col-sm-12 space-1" data-reactid="28">
                                               <div class="select select-block earning-estimation__room-type" data-reactid="29">
                                                   <select name="room_type" data-reactid="30">
                                                       <option value="entire_home" data-reactid="31">O lugar todo</option>
                                                       <option selected="" value="private_room" data-reactid="32">Quarto inteiro</option>
                                                       <option value="shared_room" data-reactid="33">Quarto compartilhado</option>
                                                   </select>
                                               </div>
                                           </div>
                                           <div class="col-md-6 col-sm-12 space-1" data-reactid="34">
                                               <div class="select select-block earning-estimation__accommodation" data-reactid="35">
                                                   <select name="person_capacity" data-reactid="36">
                                                       <option selected="" value="1" data-reactid="37">para 1 h�spede</option>
                                                       <option value="2" data-reactid="38">para 2 h�spedes</option>
                                                       <option value="3" data-reactid="39">para 3 h�spedes</option>
                                                       <option value="4" data-reactid="40">para 4 h�spedes</option>
                                                       <option value="5" data-reactid="41">para 5 h�spedes</option>
                                                       <option value="6" data-reactid="42">para 6 h�spedes</option>
                                                       <option value="7" data-reactid="43">para 7 h�spedes</option>
                                                       <option value="8" data-reactid="44">para 8 h�spedes</option>
                                                       <option value="9" data-reactid="45">para 9 h�spedes</option>
                                                       <option value="10" data-reactid="46">para 10 h�spedes</option>
                                                       <option value="11" data-reactid="47">para 11 h�spedes</option>
                                                       <option value="12" data-reactid="48">para 12 h�spedes</option>
                                                       <option value="13" data-reactid="49">para 13 h�spedes</option>
                                                       <option value="14" data-reactid="50">para 14 h�spedes</option>
                                                       <option value="15" data-reactid="51">para 15 h�spedes</option>
                                                       <option value="16" data-reactid="52">para 16 h�spedes</option>
                                                   </select>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row row-condensed space-1" data-reactid="53">
                                           <div class="col-sm-12" data-reactid="54">
                                               <input type="text" class="list-space-location-input pull-left" placeholder="ex. Campinas" name="full_address" value="Outro (Internacional)" data-reactid="55" />
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </form> -->
                           <a to="/etapa-2" class="" href="/etapa-2" data-reactid="56">
                               <button class="btn btn-babu space-top-1 text-large space-5" data-reactid="57">
                                   <span data-reactid="58">Continuar</span>
                               </button>
                           </a>
                       </div>
                       <span data-reactid="59"></span>
                   </div>
                    </div>
                    <div class="col-md-5 landing__right-col" data-reactid="60">
                        <div data-reactid="61">
                            <div class="landing__animation hide-sm lys-vertical-align-middle-container" data-reactid="62">
                                <span data-reactid="63">
                                    <div style="transition-duration:250ms;" data-reactid="64">
                                        <div class="landing__svg-container lys-vertical-align-middle" data-reactid="65"></div>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

                <?php get_footer();?>