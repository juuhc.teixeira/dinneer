<!DOCTYPE html>
<!--[if IE 9]>
  <html lang="pt"
      
      xmlns:fb="http://ogp.me/ns/fb#"
      class="ie ie9">
<![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="pt"
      
      xmlns:fb="http://ogp.me/ns/fb#">
<!--<![endif]-->
  <head>
    <meta charset="utf-8">
        <link rel="icon"  type="image/png"  href="http://www.dinneer.com/sitenovo/wp-content/uploads/2016/12/favicon.png">
        <link href="<?php echo get_bloginfo('template_url'); ?>/css/common.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/common-2.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="<?php echo get_bloginfo('template_url'); ?>/css/common-3.css" media="screen" rel="stylesheet" type="text/css" />
        <script src="https://use.fontawesome.com/ef63de7d1b.js"></script>

    

    <meta id="csrf-param-meta-tag" name="csrf-param" content="authenticity_token"/>
    <meta id="csrf-token-meta-tag" name="csrf-token" content=""/>


    <title>Alugue o Seu Quarto, Casa ou Apartamento no Airbnb</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="image_src" href="https://a0.muscache.com/airbnb/static/logos/trips-og-200x200-a3be4fbbb3b6c5e758804438dea35adc.jpg">
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Airbnb">

    <!--[if lte IE 9]>
      <script src="https://a0.muscache.com/airbnb/static/packages/libs_ie_support.bundle-89824bc88b35478e7e86.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="manifest" href="/manifest.json">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Airbnb">
    <meta name="apple-mobile-web-app-title" content="Airbnb">
    <meta name="theme-color" content="#ffffff">
    <meta name="msapplication-navbutton-color" content="#ffffff">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="msapplication-starturl" content="/?utm_source=homescreen">



  </head>
  <body class="with-new-header has-epcot-header" style="overflow-x: none">

<div class="flash-container"></div>

<main id="site-content" role="main">
      
<?php while(have_posts()){
                    the_post();
                    ?>
<div id="slash-host-container">
    <div data-hypernova-key="slash_hostbundlejs">
    <div class="slash-host-container" data-reactroot="" data-reactid="1" data-react-checksum="-1027117904">
        <div class="header" data-reactid="20">
            <div class="header__photo " data-reactid="21"></div>
            <div class="page-container-responsive header-card" data-reactid="22">
                <div class="col-sm-12 col-md-6 col-lg-5 space-md-8 space-md-top-8 text-contrast bg-babu" data-reactid="23">
                    <div class="row space-top-8 space-8" data-reactid="24">
                        <div class="col-sm-offset-1 col-sm-10 col-lg-9 col-left" data-reactid="25">
                            <h1 class="header-card__title" data-reactid="26"><strong data-reactid="27"><?php the_field('headline_imagem'); ?></strong></h1>
                            <p class="header-card__body space-top-4 space-4" data-reactid="28"><?php the_field('sub_headline_imagem'); ?></p>
                            <?php if ( is_user_logged_in() ){ ?>
                                <a href="/page-user-setting/create-activity/" class="header-card__button btn btn-primary btn-large" id="header-sign-up-button" data-reactid="29">
                                    <span data-reactid="30">Comece a Cozinhar</span>
                                </a>
                            <?php }else{ ?>
                                <a href="/page-login" class="header-card__button btn btn-primary btn-large" id="header-sign-up-button" data-reactid="29">
                                    <span data-reactid="30">Faça seu Login ou cadastre-se</span>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="earning-estimation-container" data-reactid="31">
            <div class="page-container-responsive" data-reactid="32">
                <div class="earning-estimation" data-reactid="33">
                    <div class="h2 space-4" data-reactid="34"><strong data-reactid="35"><span data-reactid="36">Quanto voc� pode ganhar</span></strong></div>
                    <div class="row earning-estimation__body" data-reactid="37">
                        <div class="col-md-3 space-sm-4" data-reactid="38">
                            <div class="room-type-selections" data-reactid="39">
                                <div class="earning-estimation__room-type" data-reactid="40"><input type="radio" checked="" data-room-type="entire_home_apt" data-reactid="41" /><span data-room-type="entire_home_apt" data-reactid="42">O lugar inteiro</span></div>
                                <div class="earning-estimation__room-type" data-reactid="43"><input type="radio" data-room-type="private_room" data-reactid="44" /><span data-room-type="private_room" data-reactid="45">Quarto inteiro</span></div>
                                <div class="earning-estimation__room-type" data-reactid="46"><input type="radio" data-room-type="shared_room" data-reactid="47" /><span data-room-type="shared_room" data-reactid="48">Quarto compartilhado</span></div>
                            </div>
                        </div>
                        <div class="col-md-4 space-sm-4" data-reactid="49">
                            <div class="space-3 earning-estimation__location-input" data-reactid="50">
                                <div data-reactid="51"><input type="text" value="" placeholder="ex. Campinas" data-reactid="52" /></div>
                            </div>
                            <div class="select select-block earning-estimation__accommodation" data-reactid="53"><select data-reactid="54"><option selected="" value="1" data-reactid="55">1 H�spede</option><option value="2" data-reactid="56">2 H�spedes</option><option value="3" data-reactid="57">3 H�spedes</option><option value="4" data-reactid="58">4 H�spedes</option><option value="5" data-reactid="59">5 H�spedes</option><option value="6" data-reactid="60">6 H�spedes</option><option value="7" data-reactid="61">7 H�spedes</option><option value="8" data-reactid="62">8 H�spedes</option><option value="9" data-reactid="63">9 H�spedes</option><option value="10" data-reactid="64">10 H�spedes</option><option value="11" data-reactid="65">11 H�spedes</option><option value="12" data-reactid="66">12 H�spedes</option><option value="13" data-reactid="67">13 H�spedes</option><option value="14" data-reactid="68">14 H�spedes</option><option value="15" data-reactid="69">15 H�spedes</option><option value="16" data-reactid="70">16+ H�spedes</option></select></div>
                        </div>
                        <div class="col-md-4" data-reactid="71">
                            <div class="va-container va-container-h va-container-v text-center earning-estimation__card" data-reactid="72">
                                <div class="va-middle" data-reactid="73">
                                    <div class="earning-estimation__amount h2" data-reactid="74"><strong data-reactid="75"> </strong></div>
                                    <div data-reactid="76"><span data-reactid="77">m�dia semanal</span><span href="#" data-behavior="tooltip" title="Estimativa baseada nos pre�os de reservas de acomoda��es em uma localiza��o semelhante � sua, na temporada, no tipo de acomoda��o e no n�mero m�ximo de visitantes. Quanto voc� ganhar� de fato pode variar de acordo com seus pre�os, o tipo e o local da sua acomoda��o, a temporada, a procura e outros fatores." class="earning-estimation__tooltip" data-reactid="78"><i class="icon icon-question-alt icon-white" data-reactid="79"></i></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="space-top-6" data-reactid="80">
            <div class="page-container-responsive" data-reactid="81">
                <div class="text-branding text-center h4 how-hosting-works__title text-muted" data-reactid="82"><span data-reactid="83"><?php the_field('section_como_funciona_title'); ?></span></div>
                <div class="how-hosting-works__section space-top-6" data-reactid="84">
                    <div data-reactid="85"><span class="how-hosting-works__section-label" data-reactid="86">1</span></div>
                    <h2 data-reactid="87"><strong data-reactid="88"><?php the_field('headline_como_funciona'); ?></strong></h2>
                    <div class="how-hosting-works__section-sub-title" data-reactid="89"><?php the_field('sub_headline_como_funciona'); ?></div>
                    <div class="row" data-reactid="90">
                        <div class="col-md-6 col-lg-4" data-reactid="91">
                            <div data-reactid="92"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_como_funciona_1_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="94"><?php the_field('campo_como_funciona_1_title');?></div>
                                <div class="info-item__body" data-reactid="95"><?php the_field('campo_como_funciona_1_text'); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="96">
                            <div data-reactid="97"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_como_funciona_2_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="99"><?php the_field('campo_como_funciona_2_title'); ?></div>
                                <div class="info-item__body" data-reactid="100"><?php the_field('campo_como_funciona_2_text'); ?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="101">
                            <div data-reactid="102"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_como_funciona_3_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="104"><?php the_field('campo_como_funciona_3_title'); ?></div>
                                <div class="info-item__body" data-reactid="105"><?php the_field('campo_como_funciona_3_text'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="how-hosting-works__section space-top-6" data-reactid="106">
                    <div data-reactid="107"><span class="how-hosting-works__section-label" data-reactid="108">2</span></div>
                    <h2 data-reactid="109"><strong data-reactid="110"><?php the_field('headline_visitantes');?></strong></h2>
                    <div class="how-hosting-works__section-sub-title" data-reactid="111"><?php the_field('sub_headline_visitantes');?></div>
                    <div class="row" data-reactid="112">
                        <div class="col-md-6 col-lg-4" data-reactid="113">
                            <div data-reactid="114"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_visitante_1_text-icon');?></span>                                
                            <div class="info-item__title space-2 space-top-2" data-reactid="116"><?php the_field('campo_visitantes_1_title');?></div>
                                <div class="info-item__body" data-reactid="117"><?php the_field('campo_visitantes_1_text');?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="118">
                            <div data-reactid="119"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_visitante_2_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="121"><?php the_field('campo_visitantes_2_title'); ?></div>
                                <div class="info-item__body" data-reactid="122"><?php the_field('campo_visitantes_2_text');?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="how-hosting-works__section space-top-6" data-reactid="123">
                    <div data-reactid="124"><span class="how-hosting-works__section-label" data-reactid="125">3</span></div>
                    <h2 data-reactid="126"><strong data-reactid="127"><?php the_field('headline_receba');?></strong></h2>
                    <div class="how-hosting-works__section-sub-title" data-reactid="128"><?php the_field('sub_headline_receba');?></div>
                    <div class="row" data-reactid="129">
                        <div class="col-md-6 col-lg-4" data-reactid="130">
                            <div data-reactid="131"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_receba_1_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="133"><?php the_field('campo_receba_1_title_');?></div>
                                <div class="info-item__body" data-reactid="134"><?php the_field('campo_receba_1_text');?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="135">
                            <div data-reactid="136"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_receba_2_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="138"><?php the_field('campo_receba_2_title');?></div>
                                <div class="info-item__body" data-reactid="139"><?php the_field('campo_receba_2_text');?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="140">
                            <div data-reactid="141"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_receba_3_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="143"><?php the_field('campo_receba_3_title');?></div>
                                <div class="info-item__body" data-reactid="144"><?php the_field('campo_receba_3_text');?></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" data-reactid="145">
                        <div class="col-md-6 col-lg-4" data-reactid="146">
                            <div data-reactid="147"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_receba_4_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="149"><?php the_field('campo_receba_4_title');?></div>
                                <div class="info-item__body" data-reactid="150"><?php the_field('campo_receba_4_text');?></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4" data-reactid="151">
                            <div data-reactid="152"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_receba_5_text-icon');?></span>
                                <div class="info-item__title space-2 space-top-2" data-reactid="154"><?php the_field('campo_receba_5_title');?></div>
                                <div class="info-item__body" data-reactid="155"><?php the_field('campo_receba_5_text');?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-container-responsive space-top-6 space-8" data-reactid="156">
            <div class="trust-and-guarantee" data-reactid="157">
                <h2 data-reactid="158"><strong data-reactid="159"><?php the_field('headline_protecao');?></strong></h2>
                <div class="text-normal trust-and-guarantee__sub-title" data-reactid="160"><?php the_field('sub_headline_protecao');?></div>
                <div class="row" data-reactid="161">
                    <div class="col-lg-3 col-md-6 col-sm-12" data-reactid="162">
                        <div data-reactid="163"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_protecao_1_text-icon');?></span>
                            <div class="info-item__title space-2 space-top-2" data-reactid="165"><?php the_field('campo_prote��o_1_title');?></div>
                            <div class="info-item__body" data-reactid="166"><?php the_field('campo_protecao_1_text');?></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12" data-reactid="167">
                        <div data-reactid="168"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_protecao_2_text-icon');?></span>
                            <div class="info-item__title space-2 space-top-2" data-reactid="170"><?php the_field('campo_prote��o_2_title');?></div>
                            <div class="info-item__body" data-reactid="171"><?php the_field('campo_protecao_2_text');?></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12" data-reactid="172">
                        <div data-reactid="173"><span class="icon-size-2 text-babu space-top-4 info-item__icon"><?php the_field('campo_protecao_3_text-icon');?></span>
                            <div class="info-item__title space-2 space-top-2" data-reactid="175"><?php the_field('campo_prote��o_3_title');?></div>
                            <div class="info-item__body" data-reactid="176"><?php the_field('campo_protecao_3_text');?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="space-top-8 hide-sm" data-reactid="177">
            <div class="video-slides air-slideshow" data-reactid="178">
                <div class="video-thumbnail-container" data-reactid="179"><span data-reactid="180"><div class="air-slide air-slide--active video-thumbnail1 background-cover" data-reactid="181"><div class="row row-table row-full-height text-center" data-reactid="182"><div class="col-middle col-md-10" data-reactid="183"><div class="h1 text-contrast" data-reactid="184"><strong data-reactid="185">Ou�a os anfitri�es, em suas pr�prias palavras</strong></div><i class="icon icon-video-play text-contrast text-jumbo" data-reactid="186"></i></div></div></div></span></div><a href="#" data-prevent-default="true" class="link-reset text-jumbo air-slideshow__prev" data-reactid="187"><i class="icon text-contrast icon-chevron-left" data-reactid="188"></i></a><a href="#" data-prevent-default="true" class="link-reset text-jumbo air-slideshow__next" data-reactid="189"><i class="icon text-contrast icon-chevron-right" data-reactid="190"></i></a></div>
            <div class="video-section" data-reactid="191">
                <div class="video-player video-player--hidden" data-reactid="192">
                    <div class="row row-table row-full-height" data-reactid="193">
                        <div class="col-sm-12 col-middle text-center" data-reactid="194"><video preload="none" class="video video--active" data-reactid="195"><source src="https://a0.muscache.com/airbnb/static/hear_from_hosts.mp4" type="video/mp4" data-reactid="196"/><source src="https://a0.muscache.com/airbnb/static/hear_from_hosts.webm" type="video/webm" data-reactid="197"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="198"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="199"><i class="icon icon-remove" data-reactid="200"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="201">
                    <div class="row row-table row-full-height" data-reactid="202">
                        <div class="col-sm-12 col-middle text-center" data-reactid="203"><video preload="none" class="video video--active" data-reactid="204"><source src="https://a0.muscache.com/airbnb/static/people_with_profiles.mp4" type="video/mp4" data-reactid="205"/><source src="https://a0.muscache.com/airbnb/static/people_with_profiles.webm" type="video/webm" data-reactid="206"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="207"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="208"><i class="icon icon-remove" data-reactid="209"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="210">
                    <div class="row row-table row-full-height" data-reactid="211">
                        <div class="col-sm-12 col-middle text-center" data-reactid="212"><video preload="none" class="video video--active" data-reactid="213"><source src="https://a0.muscache.com/airbnb/static/protection_for_your_peace_of_mind.mp4" type="video/mp4" data-reactid="214"/><source src="https://a0.muscache.com/airbnb/static/protection_for_your_peace_of_mind.webm" type="video/webm" data-reactid="215"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="216"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="217"><i class="icon icon-remove" data-reactid="218"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="219">
                    <div class="row row-table row-full-height" data-reactid="220">
                        <div class="col-sm-12 col-middle text-center" data-reactid="221"><video preload="none" class="video video--active" data-reactid="222"><source src="https://a0.muscache.com/airbnb/static/trust_makes_it_work.mp4" type="video/mp4" data-reactid="223"/><source src="https://a0.muscache.com/airbnb/static/trust_makes_it_work.webm" type="video/webm" data-reactid="224"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="225"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="226"><i class="icon icon-remove" data-reactid="227"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="228">
                    <div class="row row-table row-full-height" data-reactid="229">
                        <div class="col-sm-12 col-middle text-center" data-reactid="230"><video preload="none" class="video video--active" data-reactid="231"><source src="https://a0.muscache.com/airbnb/static/express_yourself_with_profile.mp4" type="video/mp4" data-reactid="232"/><source src="https://a0.muscache.com/airbnb/static/express_yourself_with_profile.webm" type="video/webm" data-reactid="233"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="234"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="235"><i class="icon icon-remove" data-reactid="236"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="237">
                    <div class="row row-table row-full-height" data-reactid="238">
                        <div class="col-sm-12 col-middle text-center" data-reactid="239"><video preload="none" class="video video--active" data-reactid="240"><source src="https://a0.muscache.com/airbnb/static/planning_for_arrival.mp4" type="video/mp4" data-reactid="241"/><source src="https://a0.muscache.com/airbnb/static/planning_for_arrival.webm" type="video/webm" data-reactid="242"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="243"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="244"><i class="icon icon-remove" data-reactid="245"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="246">
                    <div class="row row-table row-full-height" data-reactid="247">
                        <div class="col-sm-12 col-middle text-center" data-reactid="248"><video preload="none" class="video video--active" data-reactid="249"><source src="https://a0.muscache.com/airbnb/static/getting_paid.mp4" type="video/mp4" data-reactid="250"/><source src="https://a0.muscache.com/airbnb/static/getting_paid.webm" type="video/webm" data-reactid="251"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="252"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="253"><i class="icon icon-remove" data-reactid="254"></i></a></div>
                    </div>
                </div>
                <div class="video-player video-player--hidden" data-reactid="255">
                    <div class="row row-table row-full-height" data-reactid="256">
                        <div class="col-sm-12 col-middle text-center" data-reactid="257"><video preload="none" class="video video--active" data-reactid="258"><source src="https://a0.muscache.com/airbnb/static/reviews_help_you_improve.mp4" type="video/mp4" data-reactid="259"/><source src="https://a0.muscache.com/airbnb/static/reviews_help_you_improve.webm" type="video/webm" data-reactid="260"/></video><i class="video-player__play-icon icon icon-video-play icon-white" data-reactid="261"></i><a data-prevent-default="true" class="video-player__close link-reset" href="#" data-reactid="262"><i class="icon icon-remove" data-reactid="263"></i></a></div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="page-container-responsive footer space-8 space-top-8" data-reactid="264">
            <div class="row space-top-8" data-reactid="265">
                <div class="col-lg-offset-3 col-md-offset-2 col-sm-offset-1 col-lg-5 col-md-7 col-sm-10" data-reactid="266">
                    <div class="h1 text-white" data-reactid="267"><strong data-reactid="268"><span data-reactid="269">Prepare-se para conhecer seus visitantes</span></strong></div><a href="/rooms/new?from_sh=1" class="footer__button btn btn-primary btn-large space-top-6" id="footer-sign-up-button" data-reactid="270"><span data-reactid="271">Comece a Hospedar</span></a></div>
            </div>
            <div class="hand-key-image" data-reactid="272"></div>
        </div>
    </div>
</div>

</div>
 <?php
                }?>

    </main>

