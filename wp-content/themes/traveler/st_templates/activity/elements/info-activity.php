<?php
/**
 * @package WordPress
 * @subpackage Traveler
 * @since 1.0
 *
 * Activity element review detail
 *
 * Created by ShineTheme
 *
 */
?>

<?php
function activity_type(){
    $activity = '';
    $title = __('Activity type', ST_TEXTDOMAIN).' :';
    if($type_activity == 'daily_activity'){
        $activity = __('Daily Activity', ST_TEXTDOMAIN);
    }  else {
        $activity = __('Specific Date', ST_TEXTDOMAIN);
    }
    
    $layout = 
            '<div class="info">
                <span class="head"><br><center><i class="fa fa-info"></i></center><br>'.$title.' </span>
                <span>'.$activity.'</span>
            </div>';
    
    return $layout;
}
add_shortcode( 'activity_type', 'activity_type' );
?>
<div class="info-activity">
    <div class="info">
        <span class="head"><i class="fa fa-info"></i>$title; </span>
        <span>$activity;</span>
    </div>
    
<?php
function activity_time(){
    $activity = '';
    $activity_time = get_post_meta( get_the_ID() , 'activity-time' , true );
    if(!empty( $activity_time )){
        $title_time = st_the_language( 'activity_time' );
    }else{
        $title_time = 'Campo vazio!';
    }
    
    $layout = 
            '<div class="info">
                <span class="head"><br><center><i class="fa fa-info"></i></center><br>'.st_the_language( 'activity_time' ).' </span>
                <span>'.$title_time.'</span>
            </div>';
    
    return $layout;
}
add_shortcode( 'activity_time', 'activity_time' );
?>
<?php
    $activity_time = get_post_meta( get_the_ID() , 'activity-time' , true );
    if(!empty( $activity_time )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-clock-o"></i> <?php st_the_language( 'activity_time' ) ?> : </span>
            <span><?php echo esc_html( $activity_time ); ?> </span>
        </div>
<?php endif; ?>
    
    
<?php
function activity_duration(){
    $duration = get_post_meta( get_the_ID() , 'duration' , true );
    if(!empty( $duration )){
        $duration_time = _e('Duration',ST_TEXTDOMAIN);
    }else{
        $duration_time = 'Campo Vazio';
    }
    $layout = ' 
        <div class="info">
            <span class="head"><i class="fa fa-clock-o"></i>'.$duration.' </span>
            <span> '.esc_html( $duration_time ).' </span>
        </div>';
    
    return $layout;
}
add_shortcode( 'activity_duration', 'activity_duration' );
?>
<?php
    $hora_inicio = get_post_meta( get_the_ID(), 'hora_inicio' , true);

?>
    <div class="info">
        <span class="head"><i class="fa fa-clock-o"></i> <?php _e('Start in',ST_TEXTDOMAIN) ?> : </span>
        <span><?php echo esc_html( $hora_inicio ); ?> </span>
    </div>
<?php
    $duration = get_post_meta( get_the_ID() , 'duration' , true );
    if(!empty( $duration )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-clock-o"></i> <?php _e('Duration',ST_TEXTDOMAIN) ?> : </span>
            <span><?php echo esc_html( $duration ); ?> </span>
        </div>
<?php endif; ?>
    <div class="info">
        <span class="head"><i class="fa fa-users"></i> <?php echo __('Maximum number of people', ST_TEXTDOMAIN); ?> : </span>
        <span><?php 
            if( !$max_people || $max_people == 0 ){
                $max_people = __('Unlimited', ST_TEXTDOMAIN);
            }
            echo $max_people; 
        ?></span>
    </div>
<?php
    $facilities = get_post_meta( get_the_ID() , 'venue-facilities' , true );
    if(!empty( $facilities )):
?>
        <div class="info">
            <span class="head"><i class="fa fa-cogs"></i> <?php st_the_language( 'venue_facilities' ) ?> : </span>
            <span><?php echo esc_html( $facilities ); ?> </span>
        </div>
<?php endif; ?>
</div>