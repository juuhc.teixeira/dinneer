<header id="main-header" class="st_menu" >
    <div class="header-top <?php echo apply_filters('st_header_top_class','') ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    <a class="logo" href="<?php echo home_url('/')?>">
                        <?php
                        $logo_url = st()->get_option('logo');
                        if(!empty($logo_url)){
                            $logo = TravelHelper::get_attchment_size($logo_url , true);
                            ?>
                            <img <?php if ($logo){echo (" width='".$logo['width']."px' height ='".$logo['height']."px' ") ; } ?> src="<?php echo esc_url($logo_url); ?>" alt="logo" title="<?php bloginfo('name')?>">
                            <?php
                        }
                        ?>
                    </a>
                </div>
                <div class="col-md-5">                     
                   <div class="main_menu_wrap" id="menu1">
                        <div class="<?php echo apply_filters('st_default_menu_wrapper', "container") ; ?>" >
                            <div class="nav">
                                <?php
                                $logo__mobile_url = st()->get_option('logo_mobile',$logo_url);
                                $html_logo_mobile = "";
                                if(!empty($logo__mobile_url)){
                                    $html_logo_mobile = '<a href=\''.home_url('/').'\'><img width=auto height=40px class=st_logo_mobile src='.$logo__mobile_url.' /></a>';
                                }
                                ?>
                                <?php if(has_nav_menu('primary')){
                                    wp_nav_menu(array('theme_location'=>'primary',
                                                      "container"=>"",
                                                      'items_wrap'      => '<ul id="slimmenu" data-title="'.$html_logo_mobile.'" class="%2$s slimmenu">%3$s</ul>',
                                    ));
                                }
                                ?>
                                <script type="text/javascript">
                                    function onKey(params){
                                        if(event.keyCode = 13){
                                            var url = "http://development.dinneer.com/pagina-de-busca/";
                                            var city = "?city=" + params.value;
                                            window.location = url + city;
                                        }
                                    }
                                </script>
                                <div class="input-group menu-form">
                                    <span class="input-group-addon" id="basic-addon1"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                                    <input type="text" name="location_name" class="form-control menu-form" placeholder="City" aria-describedby="basic-addon1" onkeydown="onKey(this)">
                                </div>
                            </div>
                        </div>   
                       
<!--                        <input id="field-st-address" autocomplete="off" type="text" name="location_name" value="" class="menu-form st-location-name" placeholder="Cidade" onkeydown="onKey(this)">-->
                    </div><!-- End .main_menu_wrap-->
                </div>
                <div class="col-md-5">                 
                   
                    <?php get_template_part('users/user','nav');?>
                </div>
            </div>
        </div>
    </div> 
</header>