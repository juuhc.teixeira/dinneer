<?php
/*
 * Template Name: User Dashboard
*/

get_header();

wp_enqueue_script( 'template-user-js' );
wp_enqueue_script( 'user.js' );

$user_link = get_permalink();
$current_user = wp_get_current_user();
$lever = $current_user->roles;
$lever = array_shift($lever);
$url_id_user = '';
if(!empty( $_REQUEST[ 'id_user' ] )) {
    $id_user_tmp  = $_REQUEST[ 'id_user' ];
    $current_user = get_userdata( $id_user_tmp );
    $url_id_user  = $id_user_tmp;
}
$default_page = "setting";
if(STUser_f::check_lever_partner($lever ) and st()->get_option( 'partner_enable_feature' ) == 'on'){
    $default_page = "dashboard";
}
$sc = get_query_var( 'sc' );
if( empty( $sc ) ){
    $sc = 'dashboard';
}
?>
<?php if($sc == "details-owner"){ ?>
    <?php echo st()->load_template( 'user/user' , $sc ); ?>
<?php }else{ 

    ?>
    <div class="container-fluid bg-partner-new <?php echo esc_html($sc) ?>">
        <div class="row row_content_partner">
            <div class="col-md-3 user-left-menu ">
                <div class="page-sidebar navbar-collapse st-page-sidebar-new">
                    <ul class="page-sidebar-menu st_menu_new">
                        <li class="heading text-center user-profile-sidebar">
                            <div class="user-profile-avatar text-center">
                                <a href="<?php echo bp_loggedin_user_domain( '/' ); ?>" title="<?php the_author_meta( 'display_name', $post->post_author ); ?>">
                                    <?php if( get_the_author_meta( 'user_custom_avatar', $post->post_author ) != '' ) { ?>
                                    <img src="<?php the_author_meta( 'user_custom_avatar', $post->post_author ); ?>" alt="" width="180" />
                                        <?php echo $curauth->display_name; ?>
                                        <?php } else { 
                                            $current_user = wp_get_current_user();
                                            echo get_avatar( $current_user->user_email, 100 ); } ?>
                                </a>

                                <h5><?php echo esc_html($current_user->display_name) ?></h5>

                                <p><?php echo st_get_language('user_member_since') . mysql2date(' M Y', $current_user->data->user_registered); ?></p>
                            </div> <!-- foto e data do perfil -->
                        </li>
                        
                        <li class="item <?php if(in_array($sc,array('create-activity','edit-activity','my-activity','booking-activity', 'add-activity-booking'))) echo "active" ?>">
                            <a class="cursor">
                                <i class="fa fa-bolt"></i>
                                <?php _e( 'Activity' , ST_TEXTDOMAIN ) ?>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu item">
                                    <li <?php if($sc == 'create-activity')
                                        echo 'class="active"' ?>>
                                        <a href="/sitenovo/start/"><i
                                                class="fa fa-bolt">&nbsp;</i><?php st_the_language( 'user_create_activity' ) ?>
                                    </a>
                                </li>
                                <li <?php if($sc == 'my-activity')
                                    echo 'class="active"' ?>>
                                    <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'my-activity'); ?>"><i
                                            class="fa fa-bolt">&nbsp;</i><?php st_the_language( 'user_my_activity' ) ?>
                                    </a>
                                </li>
                                <li <?php if($sc == 'booking-activity')
                                    echo 'class="active"' ?>>
                                    <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'booking-activity'); ?>"><i
                                            class="fa fa-bolt">&nbsp;</i><?php _e( "Activity Bookings" , ST_TEXTDOMAIN ) ?>
                                    </a>
                                </li>
                            </ul>
                        </li> <!-- Opera��es com An�ncios -->
                        <?php if(!empty( $_REQUEST[ 'id_user' ] )) { ?>
                            <li class="item <?php if($sc == 'setting-info') echo 'active' ?> ">

                                <a href="<?php echo add_query_arg( 'id_user', $url_id_user ,TravelHelper::get_user_dashboared_link( $user_link, 'setting-info' )) ?>">
                                    <i class="fa fa-cog"></i>
                                    <span class="title"><?php st_the_language( 'user_settings' ) ?></span>
                                </a>
                            </li>
                        <?php }else{ ?>
                            <?php if(STUser_f::check_lever_partner( $lever ) and st()->get_option( 'partner_enable_feature' ) == 'on'): ?>
                            <?php $profilelink = '<li class="item"> <a href="' . bp_loggedin_user_domain( '/' ) . '">' . __('Seu Perfil') . '<i class="fa fa-user"></i></a></li>';
                                echo $profilelink; 
                            ?> <!-- Editar Perfil -->
                            
                            
                            
                            <?php if(STUser_f::check_lever_partner( $lever ) and st()->get_option( 'partner_enable_feature' ) == 'on'): ?>
                            
                                <?php if($lever != "administrator" && st()->get_option('enable_withdrawal', 'on') == 'on'){  ?>
                                    <li class="item <?php if(in_array($sc,array('withdrawal','withdrawal-history'))) echo "active" ?>">
                                        <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'withdrawal'); ?>">
                                            <i class="fa fa-user"></i>
                                            <?php _e("Withdrawal",ST_TEXTDOMAIN) ?>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu item">
                                            <li <?php if($sc == 'withdrawal-history') echo 'class="active"' ?>>
                                                <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'withdrawal-history'); ?>">
                                                    <i class="fa fa-clock-o">&nbsp;</i><?php _e("History",ST_TEXTDOMAIN) ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>

                                <!--<li class="item <?php /*if($sc == 'reports')echo 'active' */?>">
                                    <a href="<?php /*echo esc_url( add_query_arg( 'sc' , 'reports' , $user_link ) ) */?>"><i class="fa fa-book"></i><?php /*_e( "Reports" , ST_TEXTDOMAIN ) */?>
                                    </a>
                                </li>-->
                                    <?php if( is_super_admin() ): ?>
                                        <li class="item <?php if(in_array($sc,array('list-refund'))) echo "active" ?>">
                                            <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'list-refund'); ?>" class="cursor" style="cursor: pointer !important">
                                                <i class="fa fa-money"></i></i>
                                                <?php _e( 'Refund Manager' , ST_TEXTDOMAIN ) ?>
                                            </a>
                                        </li>    
                                    <?php endif; ?>

                            <?php endif ?>
                             
                                <li class="item <?php if($sc == 'dashboard' or $sc == 'dashboard-info') echo 'active' ?>">
                                    <a href="<?php echo TravelHelper::get_user_dashboared_link( $user_link, 'dashboard'); ?>">
                                        <i class="fa fa-cogs"></i>
                                        <span class="title"><?php _e("Dashboard",ST_TEXTDOMAIN) ?></span>
<!--                                        <span class="arrow "></span>-->
                                    </a>
                                </li>
                            <?php endif ?>
                            <li class="item <?php if($sc == 'setting') echo 'active' ?>">
                                <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'setting'); ?>"><i
                                        class="fa fa-cog"></i><?php st_the_language( 'user_settings' ) ?></a>
                            </li>
                            <?php
                            $custom_layout = st()->get_option('partner_custom_layout','off');
                            $custom_layout_booking_history = st()->get_option('partner_custom_layout_booking_history','on');
                            if($custom_layout == "off"){
                                $custom_layout_booking_history = "on";
                            }
                            ?>
                            <?php if($custom_layout_booking_history == "on"){ ?>
                            <li class="item <?php if($sc == 'booking-history') echo 'active' ?>">
                                <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'booking-history'); ?>"><i
                                        class="fa fa-clock-o"></i><?php st_the_language( 'user_booking_history' ) ?>
                                </a>
                            </li>
                            <?php } ?>
<!--                            <li class="item <?php if($sc == 'wishlist')echo 'active' ?>">
                                <a href="<?php echo TravelHelper::get_user_dashboared_link($user_link, 'wishlist'); ?>"><i
                                        class="fa fa-heart-o"></i><?php st_the_language( 'user_wishlist' ) ?></a>
                            </li>-->

                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="user-content col-md-9">
                <div class="st-page-bar">
                    <ul class="page-breadcrumb">
                        <?php echo STUser_f::st_get_breadcrumb_partner() ?>
                    </ul>
                </div>
                <?php
                if(!empty( $_REQUEST[ 'id_user' ] )) {
                    echo st()->load_template( 'user/user' , 'setting-info' , get_object_vars( $current_user ) );
                } else {
                    if(STUser_f::check_lever_partner( $lever )){
                        if(STUser_f::check_lever_service_partner($sc,$lever)){
                            switch($sc){
                                case "create-activity";
                                    $sc = "edit-activity";
                                    break;
                            }
                            echo st()->load_template( 'user/user' , $sc , get_object_vars( $current_user ) );
                        }else{
                            _e("You don't have permission to access this page",ST_TEXTDOMAIN);
                        }
                    }else{
                        if(in_array($sc,array('list-refund',"overview","setting","setting-info","wishlist","booking-history","certificate","write_review"))){
                            echo st()->load_template( 'user/user' , $sc , get_object_vars( $current_user ) );
                        }else{
                            _e("You don't have permission to access this page",ST_TEXTDOMAIN);
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
<?php } ?>
    <div class="gap"></div>
<?php
get_footer();
?>